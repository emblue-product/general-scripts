
SELECT query.id AS id, query.fecha AS fecha, query.accion AS actividad, query.actionType AS dato, query.envio AS envio FROM (
    SELECT id, accion, envio, fecha, actionType, ROW_NUMBER() OVER(ORDER BY fecha desc) as _columna_offset FROM (
    SELECT COUNT(r.reporte_abierto_id) as id  , 1 AS actionType, MAX(r.fecha) AS fecha,                    'Apertura' as accion, '' as envio FROM [5249_reporte_abiertos] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_clicks_id) as id  , 2 AS actionType, MAX(r.fecha) AS fecha,                    'Click' as accion, '' as envio FROM [5249_reporte_clicks] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_viral_id) as id  , 3 AS actionType, MAX(r.fecha) AS fecha,                    'Viral' as accion, '' as envio FROM [5249_reporte_viral] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_enviado_id) as id  , 4 AS actionType, MAX(r.fecha) AS fecha,                    'Envio' as accion, '' as envio FROM [5249_reporte_enviado] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_desuscripto_id) as id  , 5 AS actionType, MAX(r.fecha) AS fecha,                    'Desuscripción' as accion, '' as envio FROM [5249_reporte_desuscripto] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
INNER JOIN [_desuscripcion_motivo] dm on r.desuscripcion_motivo_id = dm.desuscripcion_motivo_id WHERE dm.motivo<>'Denunciante' AND r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_rebote_id) as id  , 6 AS actionType, MAX(r.fecha) AS fecha,                    'Rebote' as accion, '' as envio FROM [5249_reporte_rebote] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_suscriptos_id) as id  , 7 AS actionType, MAX(r.fecha) AS fecha,                    'Suscripción' as accion, '' as envio FROM [5249_reporte_suscriptos] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_desuscripto_id) as id  , 8 AS actionType, MAX(r.fecha) AS fecha,                    'Denuncia' as accion, '' as envio FROM [5249_reporte_desuscripto] r 
INNER JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
INNER JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
INNER JOIN [_desuscripcion_motivo] dm on r.desuscripcion_motivo_id = dm.desuscripcion_motivo_id WHERE dm.motivo='Denunciante' AND r.email_id in (15) AND r.fecha_borrado IS NULL   AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_sms_id) as id  , 9 AS actionType, MAX(r.fecha) AS fecha,                    'SMS Enviados' as accion, '' as envio FROM [5249_reporte_sms] r 
LEFT JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
LEFT JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.estado_id = 1    AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'    UNION ALL SELECT COUNT(r.reporte_sms_id) as id  , 10 AS actionType, MAX(r.fecha) AS fecha,                    'SMS Fallidos' as accion, '' as envio FROM [5249_reporte_sms] r 
LEFT JOIN [_reporte_resumen] rr on r.reporte_resumen_id=rr.reporte_resumen_id  
LEFT JOIN [_campania_elementos] ee on rr.campania_elementos_id=ee.campania_elementos_id  
WHERE r.email_id in (15) AND r.estado_id <> 1    AND r.fecha BETWEEN '2019-11-13 00:00:00.000' AND '2019-12-13 23:59:59.999'   
    ) as t1
) AS query WHERE _columna_offset BETWEEN '0' AND '25' 