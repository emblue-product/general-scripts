SELECT *
FROM [_reportes_automaticos] R 
INNER JOIN [_reportes_automaticos_tipos] T ON R.reporte_automatico_tipo_id = T.reporte_automatico_tipo_id 
LEFT JOIN [_reportes_automaticos_archivos_app] A ON A.reporte_automatico_id = R.reporte_automatico_id 
INNER JOIN [_empresa] E ON E.empresa_id = R.empresa_id
where  E.empresa_id = 19321
and r.reporte_automatico_tipo_id in( 5, 7, 9, 13)
and r.fecha_borrado is null



EMAIL,FECHA_ENVIO,FECHA_ACTIVIDAD,CAMPANIA,ACCION,TIPO_ACCION,ACTIVIDAD,DESCRIPCION[CAMPOS_PERSONALIZADOS]

SELECT e.email as 'EMAIL' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD' 
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD, ISNULL(datos.DESCRIPCION,'') AS DESCRIPCION   [CAMPOS_PERSONALIZADOS] 
FROM ( 
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Rebote' AS ACTIVIDAD, convert(varchar,RTRIM(dt.codigo),100) + ' - ' + dt.descripcion AS DESCRIPCION     
	FROM emblue3.dbo.[[EMPRESA_ID]_reporte_rebote] re INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id    
	WHERE DATEDIFF(DAY,re.fecha,GETUTCDATE()-1) = 0 
) AS datos   
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id     
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id    
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id   
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id   
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id  
INNER JOIN emblue3.dbo.[[EMPRESA_ID]_email_mundo] e  ON datos.email_id = e.email_id  
INNER JOIN emblue3.dbo.[[EMPRESA_ID]_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id  
WHERE emp.empresa_id=[EMPRESA_ID]  



select * from _reportes_automaticos_cps where reporte_automatico_id in( 249630, 249631,249632, 249634)

30	campana
36	rut
37	idcanalcampana

SELECT CPS.referencia, CAMPOS.nombre
                                            FROM [_reportes_automaticos_cps] CPS
                                            INNER JOIN 
                                            (
	                                            SELECT 
	                                            CASE WHEN CONVERT(NUMERIC(10,2),campo_numero,8) > 24 
	                                            THEN  CONVERT(VARCHAR, campo_numero, 108) 
	                                            ELSE  CONVERT(VARCHAR, campo_nombre, 108) END AS referencia,campo_nombre AS nombre
	                                            FROM [_grupos_esquema_listas] WHERE emp_id=19321
                                            ) CAMPOS --ON CAMPOS.referencia = REPLACE ( CPS.referencia , 'campo_' , '')
                                             WHERE reporte_Automatico_id =249634