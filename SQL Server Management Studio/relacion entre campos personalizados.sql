
-- relacion entre campos personalizados de usuarios y los campos personalizados de emblue
SELECT *
FROM _grupos_esquema_listas
where  emp_id = 5971
 --and campo_numero in (33,34,29)
order by campo_numero  asc





,isnull (cper.nombre, '') as 'nombre'
,isnull (cper.apellido, '') as 'apellido'
,isnull (cper.id_contacto, '') as 'id_contacto'
,isnull (cper.sexo, '') as 'sexo'
,isnull (cper.telefono_1, '') as 'telefono_1'
,isnull (cper.telefono_2, '') as 'telefono_2'
,isnull (cper.web_url, '') as 'web_url'
,isnull (cper.linkedin, '') as 'linkedin'
,isnull (cper.facebook, '') as 'facebook'
,isnull (cper.twitter, '') as 'twitter'
,isnull (cper.direccion, '') as 'direccion'
,isnull (cper.ciudad, '') as 'ciudad'
,isnull (cper.pais, '') as 'pais'
,isnull (cper.cumpleanios, '') as 'cumpleanios'
,isnull (cper.empresa, '') as 'empresa'
,isnull (cper.cargo, '') as 'cargo'
,isnull (cper.email_secundario, '') as 'email_secundario'
,isnull (cper.id_trigger, '') as 'id_trigger'
,isnull (cper.recencia, '') as 'recencia'
,isnull (cper.frecuencia, '') as 'frecuencia'
,isnull (cper.monto, '') as 'monto'
-- agrega si desea los grupos
INNER JOIN emblue3.dbo.[4615_contactos_gruposylistas] gyl on gyl.email_id = e.email_id
INNER JOIN emblue3.dbo.[_gruposylistas] gl on gyl.gruposylistas_id = gl.gruposylistas_id
------------ para crear CPs Extendidos
USE [emblue3]
GO

---INSERT INTO [dbo].[_grupos_esquema_listas]
           ([emp_id]
           ,[campo_numero]
           ,[campo_nombre]
           ,[tipo_campo]
           ,[tipo_dato]
           ,[tipo_opcion]
           ,[valores]
           ,[visible_formulario]
           ,[editable_formulario]
           ,[fijo_emblue]
           ,[solo_lectura]
           ,[campo_tabla_id]
           ,[integracion_id])
     VALUES
           (9283
           ,100
           ,'HTMLE'
           ,2
           ,3
           ,1
           ,NULL
           ,0
           ,0
           ,0
           ,0
           ,2
           ,NULL)
GO
