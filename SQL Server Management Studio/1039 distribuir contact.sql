--INSERT INTO [1039_contactos_gruposylistas] 
SELECT TOP 43598 aux.email_id, 
                 923457 
FROM  (SELECT DISTINCT( cgl.email_id ) AS email_id 
       FROM   [1039_contactos_gruposylistas] cgl 
       WHERE  cgl.gruposylistas_id IN ( 903472 )) aux 
WHERE  NOT EXISTS (SELECT email_id 
                   FROM   [1039_contactos_gruposylistas] 
                   WHERE  gruposylistas_id IN( 923456, 923457, 923458, 923459 ) 
                          AND email_id = aux.email_id) 
ORDER  BY Newid() 