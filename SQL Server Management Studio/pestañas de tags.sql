---- query pesta�a de tags de los envios

SELECT ISNULL(tl.nombre_tag,'') AS text ,tl.cantidad_clicks AS weight, COUNT(distinct r.email_id) AS unicos, r.link_id AS elementoId, l2.url, l2.nombre AS urlName, r.click_tipo_id as tipo  
FROM [_reporte_resumen] rr 
INNER JOIN [7331_reporte_clicks] r ON rr.reporte_resumen_id = r.reporte_resumen_id 
INNER JOIN [_link_relacion_tag] tl ON tl.link_id=r.link_id 
LEFT JOIN  [_link] l2 ON r.link_id = l2.link_id 
WHERE rr.campania_elementos_id IN (1345932) AND click_tipo_id = 1 
GROUP BY r.link_id,tl.nombre_tag,tl.cantidad_clicks,l2.url, l2.nombre, r.click_tipo_id 
UNION ALL 
SELECT nube.text, SUM(nube.weight) AS weight, SUM(nube.unicos) AS unicos, nube.elementoId, '' as url, '' as urlName, nube.tipo 
FROM( 
	SELECT ISNULL(tl.nombre_tag,'') AS text ,0 AS weight, COUNT(distinct CHECKSUM(r.email_id,r.link_id)) AS unicos,0 AS elementoId, r.click_tipo_id as tipo 
	FROM [_reporte_resumen] rr 
	INNER JOIN [7331_reporte_clicks] r ON rr.reporte_resumen_id = r.reporte_resumen_id 
	INNER JOIN [_link_relacion_tag] tl ON tl.link_id=r.link_id 
	LEFT JOIN  [_link] l2 ON r.link_id = l2.link_id 
	WHERE rr.campania_elementos_id IN (1345932) AND r.click_tipo_id = 1 
	GROUP BY tl.nombre_tag, r.email_id, r.link_id, r.click_tipo_id 
	UNION ALL 
	SELECT ISNULL(tl.nombre_tag,'') AS text ,COUNT(r.email_id) AS weight, 0 AS unicos, 0 AS elementoId, r.click_tipo_id as tipo 
	FROM [_reporte_resumen] rr  
	INNER JOIN [7331_reporte_clicks] r ON rr.reporte_resumen_id = r.reporte_resumen_id 
	INNER JOIN [_link_relacion_tag] tl ON tl.link_id=r.link_id 
	LEFT JOIN  [_link] l2 ON r.link_id = l2.link_id 
	WHERE rr.campania_elementos_id IN (1345932) AND r.click_tipo_id = 1  
	GROUP BY tl.nombre_tag, tl.link_id, r.click_tipo_id  
) AS nube 
GROUP BY nube.text, nube.elementoId, nube.tipo 
UNION ALL 
SELECT ISNULL(tl.nombre_tag,'') AS text ,tl.cantidad_clicks AS weight, COUNT(distinct r.email_id) AS unicos, r.link_id AS elementoId, l2.titulo as url, l2.titulo AS urlName, r.click_tipo_id as tipo  
FROM [_reporte_resumen] rr 
INNER JOIN [7331_reporte_clicks] r ON rr.reporte_resumen_id = r.reporte_resumen_id 
INNER JOIN [7331_stuff_tag] tl ON tl.stuff_id=r.link_id 
LEFT JOIN  [7331_stuff] l2 ON r.link_id = l2.stuff_id  
WHERE rr.campania_elementos_id IN (1345932) AND r.click_tipo_id = 2  
GROUP BY r.link_id,tl.nombre_tag,tl.cantidad_clicks,l2.titulo, l2.titulo, r.click_tipo_id  
UNION ALL  
SELECT nube.text, SUM(nube.weight) AS weight, SUM(nube.unicos) AS unicos, nube.elementoId, '' as url, '' as urlName, nube.tipo as tipo  
FROM( 
	SELECT ISNULL(tl.nombre_tag,'') AS text ,0 AS weight, COUNT(distinct CHECKSUM(r.email_id,r.link_id)) AS unicos,0 AS elementoId, click_tipo_id as tipo  
	FROM [_reporte_resumen] rr  
	INNER JOIN [7331_reporte_clicks] r ON rr.reporte_resumen_id = r.reporte_resumen_id  
	INNER JOIN [7331_stuff_tag] tl ON tl.stuff_id=r.link_id  
	LEFT JOIN  [7331_stuff] l2 ON r.link_id = l2.stuff_id  
	WHERE rr.campania_elementos_id IN (1345932) AND r.click_tipo_id = 2  
	GROUP BY tl.nombre_tag, r.email_id, r.link_id, r.click_tipo_id  
	UNION ALL  
	SELECT ISNULL(tl.nombre_tag,'') AS text ,COUNT(r.email_id) AS weight, 0 AS unicos, 0 AS elementoId, click_tipo_id as tipo 
	FROM [_reporte_resumen] rr 
	INNER JOIN [7331_reporte_clicks] r ON rr.reporte_resumen_id = r.reporte_resumen_id  
	INNER JOIN [7331_stuff_tag] tl ON tl.stuff_id=r.link_id  
	LEFT JOIN  [7331_stuff] l2 ON r.link_id = l2.stuff_id  
	WHERE rr.campania_elementos_id IN (1345932) AND r.click_tipo_id = 2  
	GROUP BY tl.nombre_tag, tl.stuff_id, r.click_tipo_id  
) AS nube  
GROUP BY nube.text, nube.elementoId, nube.tipo 