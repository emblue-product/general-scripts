BEGIN
          DECLARE @consultaSQL VARCHAR(max) 
		  DECLARE @empresa_id VARCHAR(40)
		  DECLARE @datooriginal VARCHAR(max)

          DECLARE permitirDesuscriptos CURSOR FOR 
            
				select distinct(empresa_id)  
				from _configuracion_general 
				where empresa_id not in (
								select empresa_id  
								from _configuracion_general 
								where config_descripcion = 'permitir_no_destinatarios_sms')   
				order by empresa_id desc;
			 
          OPEN permitirDesuscriptos 

          FETCH next FROM permitirDesuscriptos INTO @empresa_id

          WHILE @@FETCH_STATUS = 0 
            BEGIN 

				SET @consultaSQL = 'INSERT INTO [dbo].[_configuracion_general] ([config_descripcion],[config_valor],[empresa_id]) VALUES (''permitir_no_destinatarios_sms''  ,''1'' ,'+ @empresa_id + ')'
				
				
				PRINT @consultaSQL

                EXEC(@consultaSQL) 

                FETCH next FROM permitirDesuscriptos INTO @empresa_id
            END 

          CLOSE permitirDesuscriptos 
          DEALLOCATE permitirDesuscriptos 
END