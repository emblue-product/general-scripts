-- touch rules

-- trigger unicos
SELECT COUNT(reporte_enviado_id) AS enviados FROM [228_reporte_enviado]
                     WHERE fecha >= GETUTCDATE()-20 AND email_id = 169142


-- trigger lotes

UPDATE tac 
SET    tac.elementos_estados_id = 12 
FROM   [228_trigger_actividad_contacto] tac 
       INNER JOIN (
	   SELECT r.email_id 
                   FROM   [228_reporte_enviado] r 
                   WHERE  r.fecha >= Getdate() - 2 
                   GROUP  BY email_id 
                   HAVING Count(r.reporte_enviado_id) >= 1
				   ) excluir 
               ON tac.contacto_id = excluir.email_id 
WHERE  trigger_actividad_contacto_id IN (465,466,474,475) 



SELECT *--tac.elementos_estados_id
FROM   [228_trigger_actividad_contacto] tac 
       INNER JOIN (
	   SELECT r.email_id 
                   FROM   [228_reporte_enviado] r 
                   WHERE  r.fecha >= Getdate() - 2 
                   GROUP  BY email_id 
                   HAVING Count(r.reporte_enviado_id) >= 1
				   ) excluir 
               ON tac.contacto_id = excluir.email_id 
WHERE  tac.trigger_actividad_contacto_id IN (465,466,474,475) 




select top 10 * FROM [228_trigger_actividad_contacto]  
WHERE trigger_actividad_contacto_id in (465,466,474,475,476)  --IN ( 169142, 169143 ) 
order by fecha_envio desc

update [228_trigger_actividad_contacto] set contacto_id = 169142 where trigger_actividad_contacto_id in ( 169142, 169143) 



SELECT trigger_actividad_contacto_id 
FROM [228_trigger_actividad_contacto] tac
WHERE tac.elementos_estados_id <> 12 AND tac.trigger_actividad_contacto_id IN (169142,169143)



USE [emblue3_new]
GO

SELECT top 10 [elementos_envio_trigger_actividad_id]
      ,[elementos_envio_id]
      ,[trigger_actividad_desencadenante_id]
      ,[campo_id]
      ,[trigger_actividad_tipo_id]
      ,[trigger_actividad_acumulada_cantidad]
      ,[trigger_key]
      ,[trigger_actividad_redireccionamiento_final_id]
      ,[url_destino]
      ,[landing_page_id]
      ,[fecha_envio]
      ,[hora_envio]
      ,[enviar_cada_anio]
      ,[activo]
      ,[trigger_actividad_redireccionamiento_final_id_error]
      ,[url_destino_error]
      ,[landing_page_id_error]
      ,[gruposylistas_id]
      ,[envio_recurrente]
      ,[activo_indefinido]
      ,[fecha_cierre]
      ,[group_rules]
      ,[group_rules_in]
  FROM [dbo].[_elementos_envio_trigger_actividad]
  where [elementos_envio_id] in( 7083)
GO





select touch_rules_dias, touch_rules_emails FROM _empresa WHERE empresa_id = 228

--update _empresa set  touch_rules_dias= 1 , touch_rules_emails = 1 WHERE empresa_id = 228



USE [emblue3]
GO

SELECT [trigger_actividad_tipo_id]
      ,[nombre]
  FROM [dbo].[_trigger_actividad_tipo]
GO

USE [emblue3]
GO

SELECT [trigger_actividad_desencadenante_id]
      ,[nombre]
  FROM [dbo].[_trigger_actividad_desencadenante]
GO

