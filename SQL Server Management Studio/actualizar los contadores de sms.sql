BEGIN
          DECLARE @consultaSQL VARCHAR(max) 
		  DECLARE @reporte_resumen_id VARCHAR(40)
		  DECLARE @datooriginal VARCHAR(max)

          DECLARE reporte_sms CURSOR FOR 
            
					select rr.reporte_resumen_id
					from _campania_elementos ce
						inner join [_campania] c on ce.campania_id = c.campania_id
						inner join _reporte_resumen rr on ce.campania_elementos_id = rr.campania_elementos_id
					where c.emp_id = 5165
						and ce.elementos_tipo_id in (8, 5)
						and ce.fecha_creacion between '2019-10-01' and '2020-05-10'
						and ce.elementos_estados_id in (4, 5, 18)
						and ce.fecha_borrado is null
						--and ce.cantidad_destinatarios > 29
			 
          OPEN reporte_sms 

          FETCH next FROM reporte_sms INTO @reporte_resumen_id

          WHILE @@FETCH_STATUS = 0 
            BEGIN 

				SET @consultaSQL = 'update _reporte_resumen '
				+ ' set cantidad_sms_totales = (select count(reporte_sms_id) from [5165_reporte_sms] where reporte_resumen_id = ' + @reporte_resumen_id + '), '
				  + ' cantidad_sms_enviados = (select count(reporte_sms_id) from [5165_reporte_sms] where reporte_resumen_id = ' + @reporte_resumen_id + ' and infobip_estado_id = 5), '
				  + ' cantidad_sms_fallidos = (select count(reporte_sms_id) from [5165_reporte_sms] where reporte_resumen_id = ' + @reporte_resumen_id + ' and infobip_estado_id <> 5 and infobip_estado_id <> 7), '
				  + ' amount_pending_sms = (select count(reporte_sms_id) from [5165_reporte_sms] where reporte_resumen_id = ' + @reporte_resumen_id + ' and infobip_estado_id = 7) '
				+ ' where reporte_resumen_id = ' + @reporte_resumen_id + ''
				PRINT @consultaSQL

                EXEC(@consultaSQL) 

                FETCH next FROM reporte_sms INTO @reporte_resumen_id
            END 

          CLOSE reporte_sms 
          DEALLOCATE reporte_sms 
END