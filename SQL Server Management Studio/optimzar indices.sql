Use emblue3

Declare @TBname nvarchar(255)
Declare @SQL nvarchar(1000)

DECLARE cursort CURSOR FOR 
SELECT TABLE_NAME from INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' and table_name like '30156_%'
OPEN cursort 
FETCH next FROM cursort INTO @TBname 

WHILE @@FETCH_STATUS = 0 
BEGIN 
    set @SQL='ALTER INDEX ALL ON [' + @TBname + '] REBUILD;'
    print @SQL
    EXEC SP_EXECUTESQL @SQL   
	
	FETCH next FROM cursort INTO @TBname 
END
CLOSE cursort 
DEALLOCATE cursort 
PRINT 'Finaliza Proceso'