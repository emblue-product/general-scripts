SELECT aux2.email_id,aux2.email, aux2.cantidad, nombre, apellido  
FROM ( 
	SELECT * 
	FROM (  	
		SELECT  aux.email_id,email.email,cantidad, ROW_NUMBER () OVER (ORDER BY email asc ) AS fila  	
		FROM (  		
			SELECT reporte.email_id,  COUNT( distinct reporte.reporte_resumen_id) AS cantidad  		
			FROM [2731_reporte_abiertos] reporte     		INNER JOIN [_reporte_resumen] rr ON reporte.reporte_resumen_id = rr.reporte_resumen_id     		
			WHERE ( (reporte.fecha BETWEEN '2019-01-31T00:00:00.000' AND '2019-02-12T23:59:59.000') )   		AND rr.campania_elementos_id IN (810648)  
			AND reporte.email_id  NOT IN   	(  	    
				SELECT reporte2.email_id   	    
				FROM [2731_reporte_abiertos] reporte2      	  	    
					INNER JOIN [_reporte_resumen] rr2 on reporte2.reporte_resumen_id = rr2.reporte_resumen_id
				WHERE (reporte2.fecha < /*DESDE*/ '2019-01-31T00:00:00.000') AND rr2.campania_elementos_id = rr2.campania_elementos_id AND rr2.campania_elementos_id in (810648)
				) 	  		GROUP BY reporte.email_id  			  		) AS aux  	INNER JOIN [2731_email_mundo] email ON aux.email_id = email.email_id  	
				WHERE email.email LIKE '%%'   	GROUP BY aux.email_id,email.email,cantidad      ) AS pages      

WHERE pages.fila BETWEEN 1 AND 20  ) AS aux2          INNER JOIN [2731_contactos_campos_personalizados_chico] campo on campo.email_id = aux2.email_id    ORDER BY fila ASC;


