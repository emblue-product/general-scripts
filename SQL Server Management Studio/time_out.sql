--INSERT INTO [temp_importacion_email_ids_940625]
SELECT insercionEmails.email_id, NULL AS row_number FROM (
SELECT DISTINCT aux.* ,  CASE WHEN ISNULL(cp.cuarentena, 0) >= 1 OR ISNULL(cp.desuscripto, 0) >= 1 OR ISNULL(cp.lista_negra, 0) >= 1 OR ISNULL(cp.denunciante, 0) >= 1 OR ISNULL(cp.inseguro, 0) >= 1
    THEN 1  WHEN cp.ultima_actividad IS NOT NULL THEN
    CASE WHEN cp.ultima_actividad IS NOT NULL AND  DATEDIFF(DAY, cp.ultima_actividad, GETUTCDATE()) <= 15
    THEN 7
    WHEN cp.ultima_actividad IS NOT NULL AND  DATEDIFF(DAY, cp.ultima_actividad, GETUTCDATE()) <= 45
    THEN 6
    WHEN cp.ultima_actividad IS NOT NULL AND  DATEDIFF(DAY, cp.ultima_actividad, GETUTCDATE()) <= 120
    THEN 5
    ELSE 4
END
WHEN cp.fecha_creacion IS NOT NULL AND  DATEDIFF(DAY, cp.fecha_creacion, GETUTCDATE()) <= 120
THEN 3  ELSE 2 END  AS rank
FROM (
SELECT DISTINCT (CASE WHEN (CHARINDEX('+',aux.email_aux,0)) > 0
THEN SUBSTRING(aux.email_aux,0,CHARINDEX('+',aux.email_aux,0)) + SUBSTRING(aux.email_aux,CHARINDEX('@',aux.email_aux,0), LEN(aux.email_aux))
ELSE aux.email_aux END) AS email, aux.email_aux , aux.email_id
FROM (SELECT distinct [dbo].[Trim1](e.email) AS email_aux, e.email_id
FROM _elemento_envio_gruposylistas eg INNER JOIN [893_contactos_gruposylistas] cg
WITH(NOLOCK) ON eg.gruposylistas_id = cg.gruposylistas_id INNER JOIN [893_email_mundo] e
WITH(NOLOCK) ON cg.email_id = e.email_id
where eg.tipo_gruposylistas IN (2,7) AND eg.elementos_envio_id = 938343 and e.activo = 1 ) AS aux ) AS aux
INNER JOIN (
    select email_id  ,
    sum(cast(cuarentena as int)) as cuarentena,
    sum(isnull(desuscripto,0)) as desuscripto,
    sum(cast(lista_negra as int)) as lista_negra,
    sum(cast(denunciante as int)) as denunciante,
    sum(cast(inseguro as int)) as inseguro,
    max(ultima_actividad) as ultima_actividad,
    min(fecha_creacion) as fecha_creacion
    from [893_contactos_propiedades] WITH(NOLOCK)
    group by email_id) cp ON aux.email_id = cp.email_id
WHERE
ISNULL(cp.desuscripto, 0) = 0 AND
ISNULL(cp.cuarentena, 0) = 0 AND
ISNULL(cp.denunciante, 0)= 0 AND
ISNULL(cp.inseguro, 0) = 0 AND
ISNULL(cp.lista_negra, 0) = 0  )
AS insercionEmails ORDER BY insercionEmails.rank desc

sp_whoisactive