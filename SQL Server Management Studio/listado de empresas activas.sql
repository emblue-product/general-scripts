
-- listado de empresas

SELECT			empresa1_.empresa_id    AS col_0_0_, 
                empresa1_.nombre        AS col_1_0_, 
                empresa1_.fecha_borrado AS col_2_0_, 
                agencia3_.contrato      AS col_3_0_, --COUNT (agencia3_.contrato )
                agencia3_.nombre        AS col_4_0_, 
				                agencia3_.agencia_id        AS col_4_0_1, 
                agenciaest4_.estado_id  AS col_5_0_ 
FROM   _nodo_seguridad nodoempres0 
       LEFT OUTER JOIN _empresa empresa1_ 
                    ON nodoempres0.empresa_id = empresa1_.empresa_id 
       LEFT OUTER JOIN _nodo_seguridad nodoagenci2_ 
                    ON nodoempres0.nodo_agencia_id = 
                       nodoagenci2_.nodo_seguridad_id 
       LEFT OUTER JOIN _agencia agencia3_ 
                    ON nodoagenci2_.agencia_id = agencia3_.agencia_id 
       LEFT OUTER JOIN _agencia_estados agenciaest4_ 
                    ON agencia3_.estado_id = agenciaest4_.estado_id 
WHERE  nodoempres0.nodo_tipo = 'empresa' 
       --AND ( agenciaest4_.estado_id IN ( 1 ) ) 
       AND ( agencia3_.fecha_borrado IS NULL ) 
	   AND ( empresa1_.fecha_borrado IS NULL )
	   and empresa1_.empresa_id in ( 5971, 1140)
--GROUP BY agencia3_.contrato ORDER BY COUNT (agencia3_.contrato ) DESC

 select  * from _agencia_estados




 --- usuarios de la empresa

 SELECT          empresa0_.empresa_id               AS empresa1_0_1_, 
                empresa0_.nombre                   AS nombre0_1_, 
                empresa0_.fecha_borrado            AS fecha3_0_1_, 
                empresa0_.razon_social             AS razon4_0_1_, 
                empresa0_.observaciones            AS observac5_0_1_, 
                empresa0_.cuit                     AS cuit0_1_, 
                empresa0_.rubro                    AS rubro0_1_, 
                empresa0_.direccion                AS direccion0_1_, 
                empresa0_.localidad                AS localidad0_1_, 
                empresa0_.telefono                 AS telefono0_1_, 
                empresa0_.email                    AS email0_1_, 
                empresa0_.web                      AS web0_1_, 
                empresa0_.responsable              AS respons13_0_1_, 
                empresa0_.time_offset              AS time14_0_1_, 
                empresa0_.idioma                   AS idioma0_1_, 
                empresa0_.host_id                  AS host16_0_1_, 
                empresa0_.touch_rules_dias         AS touch17_0_1_, 
                empresa0_.touch_rules_emails       AS touch18_0_1_, 
                empresa0_.biflow_estado_id         AS biflow19_0_1_, 
                empresa0_.permite_adjuntos         AS permite20_0_1_, 
                empresa0_.onsite                   AS onSite0_1_, 
                empresa0_.maximo_grupos            AS maximo22_0_1_, 
                empresa0_.maximo_tags              AS maximo23_0_1_, 
                empresa0_.maximo_perfiles          AS maximo24_0_1_, 
                empresa0_.email_concatenar         AS email25_0_1_, 
                empresa0_.contacto_prueba_id       AS contacto26_0_1_, 
                configurac1_.configuracion_rank_id AS configur1_16_0_, 
                configurac1_.empresa_id            AS empresa2_16_0_, 
                configurac1_.cantidad_aperturas    AS cantidad3_16_0_, 
                configurac1_.ranking_aperturas     AS ranking4_16_0_, 
                configurac1_.cantidad_clicks       AS cantidad5_16_0_, 
                configurac1_.ranking_clicks        AS ranking6_16_0_, 
                configurac1_.cantidad_viral        AS cantidad7_16_0_, 
                configurac1_.ranking_viral         AS ranking8_16_0_, 
                configurac1_.ranking_suscripto     AS ranking9_16_0_, 
                configurac1_.ranking_verificado    AS ranking10_16_0_, 
                configurac1_.ranking_descartados   AS ranking11_16_0_, 
                configurac1_.configuracion_defecto AS configu12_16_0_ 
FROM            _empresa empresa0_ 
LEFT OUTER JOIN _configuracion_rank configurac1_ 
ON              empresa0_.empresa_id=configurac1_.empresa_id 
WHERE           empresa0_.empresa_id in( 24351)




-----------------------------------

SELECT nodoagenci0_.agencia_id        AS agencia5_1_, 
       nodoagenci0_.nodo_seguridad_id AS nodo1_1_
       --nodoagenci0_.nodo_seguridad_id AS nodo1_8_0_, 
       --nodoagenci0_.agencia_id        AS agencia5_8_0_ 
FROM   _nodo_seguridad nodoagenci0_ 
WHERE  nodoagenci0_.agencia_id = 100 in ( 19724,19725,19726)

19724	44075
19725	44076
19726	44077

-----------------------------------------


SELECT usuarios0_.nodo_seguridad_id      AS nodo3_1_, 
       usuarios0_.nodo_seguridad_usuario_id   AS nodo1_1_, 
       usuarios0_.usuario_id                AS usuario2_9_0_, 
       usuarios0_.usuario_rol_id            AS usuario4_9_0_ 
FROM   _nodo_seguridad_usuario usuarios0_ 
WHERE  usuarios0_.usuario_id = 11640 -- este nodo busca al admin de la cuenta


SELECT usuarios0_.nodo_seguridad_id         AS nodo3_1_, 
       usuarios0_.nodo_seguridad_usuario_id AS nodo1_1_, 
       usuarios0_.usuario_id                AS usuario2_9_0_, 
       usuarios0_.usuario_rol_id            AS usuario4_9_0_ 
FROM   _nodo_seguridad_usuario usuarios0_ 
WHERE  usuarios0_.nodo_seguridad_id= 1 -- este nodo busca a los usuarios comunes de la cuenta



SELECT *
FROM   _nodo_seguridad 
WHERE  nodo_agencia_id is not null

SELECT  *
FROM   _nodo_seguridad 
WHERE   nodo_agencia_id = 4058

SELECT  *
FROM   _nodo_seguridad 
WHERE   nodo_seguridad_id = 4058


------------------------------- devuelve las empresas


SELECT  u.email, emp.empresa_id, ag.contrato
FROM [_usuario]  u
INNER JOIN [_nodo_seguridad_usuario] nsu ON nsu.usuario_id = u.usuario_id
INNER JOIN [_nodo_seguridad] nsa ON nsu.nodo_seguridad_id = nsa.nodo_seguridad_id
INNER JOIN [_nodo_seguridad] nse ON nse.nodo_agencia_id = nsa.nodo_seguridad_id
INNER JOIN [_empresa] emp ON nse.empresa_id = emp.empresa_id
INNER JOIN [_agencia] ag ON nsa.agencia_id = ag.agencia_id
WHERE 1=1
--and emp.nombre like 'Nec+%'
--and u.fecha_borrado is null
--and emp.fecha_borrado is null
and u.email in ('facundocallovi@hotmail.com')