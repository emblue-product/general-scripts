SELECT emails.email_inseguro
	, COUNT(emails.email_inseguro) AS total
	, SUM(emails.generico) as generico
	, SUM(emails.anulable) as anulable 
FROM (
		SELECT i.email_inseguro, 0 AS generico, 0 AS anulable FROM [_email_inseguros] i WHERE i.emp_id = {id_empresa}
		UNION ALL
		SELECT i.email_generico as email_inseguro, 1 AS generico, i.anulable FROM [_email_genericos] i 
	) AS emails
GROUP BY emails.email_inseguro



SELECT * FROM [_email_genericos] where email_generico like '%spamcop.net%'


--update [_email_genericos] set email_generico = '*spamcop*' where email_genericos_id = 117
