                    SELECT
                        ta.elementos_envio_trigger_actividad_id,
                        ee.campanias_elementos_id AS elemento_id,
                        c.emp_id AS empresa_id,
                        ta.fecha_envio,
                        g.gruposylistas_id
                    FROM
                        _elementos_envio_trigger_actividad ta
                            LEFT JOIN _gruposylistas g ON
                                g.gruposylistas_id = ta.gruposylistas_id
                            INNER JOIN _trigger_actividad_desencadenante tad ON
                                tad.trigger_actividad_desencadenante_id = ta.trigger_actividad_desencadenante_id
                            INNER JOIN _elementos_envio ee ON
                                ee.elementos_envio_id = ta.elementos_envio_id
                            INNER JOIN _campania_elementos ce ON
                                ce.campania_elementos_id = ee.campanias_elementos_id
                            INNER JOIN _elementos_tipos et ON
                                et.elementos_tipos_id = ce.elementos_tipo_id
                            INNER JOIN _campania c ON
                                c.campania_id = ce.campania_id
                    WHERE
                        tad.nombre = 'Api Integrador'
                        and et.descripcion = 'Trigger'
                        and ce.fecha_borrado is null
                        and c.fecha_borrado is null and ta.activo = 1						
                        and (ta.activo_indefinido = 1 or ta.fecha_cierre >= getdate())
                        and ce.campania_elementos_id = 1301749



USE [emblue3]
GO

SELECT [reporte_adjunto_id]
      ,[reporte_resumen_id]
      ,[email_id]
      ,[tipo_id]
      ,[estado_id]
      ,[extension_id]
      ,[bytes]
      ,[adjunto]
      ,[nombre]
      ,[email]
      ,[ultima_modificacion]
      ,[fecha]
  FROM [dbo].[7331_reporte_adjuntos] where [reporte_resumen_id] = 1048196
GO
