-- Acciones con 0 open
SELECT ce.campania_elementos_id as 'ID'
	, ce.nombre as 'Accion'
	, ce.elementos_estados_id 
	, ce.cantidad_destinatarios as 'Destinatarios'
	, DATEADD(hh, -3, rr.ultimo_enviado) as 'Fecha Ultimo Envio'
	, datediff(minute, rr.ultimo_enviado, getutcdate())  as 'SLA (min)'
	, rr.cantidad_aperturas AS Aperturas
FROM _campania_elementos ce
	INNER JOIN _reporte_resumen rr ON rr.campania_elementos_id = ce.campania_elementos_id
WHERE DATEDIFF(day, DATEADD(hh, -3, rr.ultimo_enviado), DATEADD(hh, -3, GETUTCDATE())) = 0 
	and ce.elementos_estados_id  in (15,16,17,7,4,8,11,9,10,5,12,6,13,14,21) 
	and rr.cantidad_aperturas = 0 
	and ce.elementos_tipo_id in (1, 9)
	and rr.cantidad_enviados > 500
	and datediff(minute, rr.ultimo_enviado, getutcdate()) > 20
ORDER BY  rr.ultimo_enviado ASC

-- para emanuel

SELECT Count(ce.campania_elementos_id)
FROM _campania_elementos ce
	INNER JOIN _reporte_resumen rr ON rr.campania_elementos_id = ce.campania_elementos_id
WHERE DATEDIFF(day, DATEADD(hh, -3, rr.ultimo_enviado), DATEADD(hh, -3, GETUTCDATE())) = 0 
	and ce.elementos_estados_id  in (15,16,17,7,4,8,11,9,10,5,12,6,13,14,21) 
	and rr.cantidad_aperturas = 0 
	and ce.cantidad_destinatarios > 200
	and ce.elementos_tipo_id in (1, 9)


-- ID de Estados Elementos
SELECT * FROM _elementos_estados

--select * from _campania_elementos where 

---- la oficial del cheff

SELECT campania_elementos_id, count(campania_elementos_id) FROM ( SELECT e.campania_elementos_id, e.nombre AS envio, c.nombre AS campania, c.campania_id, em.nombre AS empresa, en.fecha_envio, ee.descripcion AS estado, te.descripcion AS tipo, 
er.fecha_inicio, ere.cantidad_enviados AS enviados, ere.cantidad_aperturas AS abiertos, ere.cantidad_clicks AS clicks, ere.cantidad_rebotados AS rebotes,
em.empresa_id, ere.reporte_resumen_id, 
(CASE WHEN ere.cantidad_enviados IS NOT NULL AND ere.cantidad_enviados > 0 AND ere.cantidad_aperturas IS NOT NULL THEN ((ere.cantidad_aperturas * 100) / ere.cantidad_enviados) ELSE 0 END) AS open_rate,
hb.return_path_descripcion,e.cantidad_destinatarios, CASE WHEN eeld.elementos_envio_listas_datacrush_id IS NOT NULL THEN 1 ELSE 0 END data_crush
FROM _campania_elementos as e 
INNER JOIN _campania c ON c.campania_id = e.campania_id 
INNER JOIN _elementos_estados ee ON ee.elementos_estados_id = e.elementos_estados_id 
LEFT JOIN _reporte_resumen ere ON ere.campania_elementos_id = e.campania_elementos_id
LEFT JOIN _elementos_envio en ON en.campanias_elementos_id = e.campania_elementos_id
LEFT JOIN _elementos_envio_refuerzos er ON er.elementos_envio_id = en.elementos_envio_id
INNER JOIN _elementos_tipos te ON te.elementos_tipos_id = e.elementos_tipo_id 
INNER JOIN _empresa em ON em.empresa_id = c.emp_id
LEFT JOIN _historico_balanceo hb ON hb.campania_elementos_id = e.campania_elementos_id
LEFT JOIN _elemento_envio_listas_datacrush eeld ON en.elementos_envio_id = eeld.elementos_envio_id
WHERE e.fecha_borrado is null AND e.elementos_tipo_id IN (1,9) AND c.fecha_borrado is null  AND ee.elementos_estados_id IN (3,15,16,17,7,4,8,11,9,10,5,12,6,13,14,21)  AND DATEDIFF(day, en.fecha_envio, GETUTCDATE()) = 0
GROUP BY e.campania_elementos_id, e.nombre, c.nombre, c.campania_id, em.nombre, en.fecha_envio, ee.descripcion, te.descripcion, er.fecha_inicio, 
ere.cantidad_enviados, ere.cantidad_aperturas, ere.cantidad_clicks, ere.cantidad_rebotados, em.empresa_id, ere.reporte_resumen_id, hb.return_path_descripcion,e.cantidad_destinatarios,eeld.elementos_envio_listas_datacrush_id ) AS aux 

 group by campania_elementos_id
 having count(campania_elementos_id)>1

 --- para los split
SELECT DISTINCT * FROM ( SELECT e.campania_elementos_id, e.nombre AS envio, c.nombre AS campania, c.campania_id, em.nombre AS empresa, en.fecha_envio, ee.descripcion AS estado, te.descripcion AS tipo, 
er.fecha_inicio, ere.cantidad_enviados AS enviados, ere.cantidad_aperturas AS abiertos, ere.cantidad_clicks AS clicks, ere.cantidad_rebotados AS rebotes,
em.empresa_id, ere.reporte_resumen_id, 
(CASE WHEN ere.cantidad_enviados IS NOT NULL AND ere.cantidad_enviados > 0 AND ere.cantidad_aperturas IS NOT NULL THEN ((ere.cantidad_aperturas * 100) / ere.cantidad_enviados) ELSE 0 END) AS open_rate,
hb.return_path_descripcion,e.cantidad_destinatarios, CASE WHEN eeld.elementos_envio_listas_datacrush_id IS NOT NULL THEN 1 ELSE 0 END data_crush
FROM _campania_elementos as e 
INNER JOIN _campania c ON c.campania_id = e.campania_id 
INNER JOIN _elementos_estados ee ON ee.elementos_estados_id = e.elementos_estados_id 
LEFT JOIN _reporte_resumen ere ON ere.campania_elementos_id = e.campania_elementos_id
LEFT JOIN _elementos_envio en ON en.campanias_elementos_id = e.campania_elementos_id
LEFT JOIN _elementos_envio_refuerzos er ON er.elementos_envio_id = en.elementos_envio_id
INNER JOIN _elementos_tipos te ON te.elementos_tipos_id = e.elementos_tipo_id 
INNER JOIN _empresa em ON em.empresa_id = c.emp_id
LEFT JOIN _historico_balanceo hb ON hb.campania_elementos_id = e.campania_elementos_id
LEFT JOIN _elemento_envio_listas_datacrush eeld ON en.elementos_envio_id = eeld.elementos_envio_id
WHERE e.fecha_borrado is null AND e.elementos_tipo_id IN (1,9) AND c.fecha_borrado is null  AND ee.elementos_estados_id IN (15,16,17,7,4)  AND DATEDIFF(day, en.fecha_envio, GETUTCDATE()) = 0 --en.fecha_envio = BETWEEN '2019-07-11 00:00:00' AND '2019-07-11 23:59:59'
GROUP BY e.campania_elementos_id, e.nombre, c.nombre, c.campania_id, em.nombre, en.fecha_envio, ee.descripcion, te.descripcion, er.fecha_inicio, 
ere.cantidad_enviados, ere.cantidad_aperturas, ere.cantidad_clicks, ere.cantidad_rebotados, em.empresa_id, ere.reporte_resumen_id, hb.return_path_descripcion,e.cantidad_destinatarios,eeld.elementos_envio_listas_datacrush_id ) AS aux 
 ORDER BY fecha_envio DESC