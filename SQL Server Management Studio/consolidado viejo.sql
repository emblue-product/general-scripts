SELECT c.nombre as CAMPANIA, 
ce.nombre as ACCION
, t.descripcion TIPO
, en.asunto as ASUNTO
, en.email_from as SENDER  
, ce.touch_rules as TOUCHRULES 
, rr.fecha as FECHA 
, ce.cantidad_destinatarios as DESTINATARIOS
, rr.cantidad_enviados AS ENVIADOS
, rr.cantidad_rebotados AS REBOTES
, (rr.cantidad_enviados - rr.cantidad_rebotados) as EFECTIVOS 
, rr.cantidad_aperturas AS ABIERTOS
, rr.cantidad_clicks AS CLICKS
, rr.cantidad_viral AS VIRALES
, rr.cantidad_suscriptos AS SUSCRIPTOS
, rr.cantidad_desuscriptos  AS DESUSCRIPTOS  


, CASE WHEN (rr.cantidad_enviados - rr.cantidad_rebotados) > 0 and rr.cantidad_enviados > 0 THEN CAST((((rr.cantidad_enviados - rr.cantidad_rebotados)*100)/rr.cantidad_enviados) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'DR'  
, CASE WHEN (rr.cantidad_enviados - rr.cantidad_rebotados) > 0 and rr.cantidad_rebotados > 0 THEN CAST((((rr.cantidad_enviados - rr.cantidad_rebotados)*100)/rr.cantidad_rebotados) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'BR' 
, CASE WHEN (rr.cantidad_enviados - rr.cantidad_rebotados) > 0 and rr.cantidad_aperturas > 0 THEN CAST(((rr.cantidad_aperturas*100)/(rr.cantidad_enviados - rr.cantidad_rebotados)) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'OR' 
--, CASE WHEN (rr.cantidad_enviados - rr.cantidad_rebotados) > 0 and rr.cantidad_aperturas_U > 0 THEN CAST(((rr.cantidad_aperturas_U*100)/(rr.cantidad_enviados - rr.cantidad_rebotados)) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'UOR' 
, CASE WHEN (rr.cantidad_enviados - rr.cantidad_rebotados) > 0 and rr.cantidad_clicks > 0 THEN CAST(((rr.cantidad_clicks*100)/(rr.cantidad_enviados - rr.cantidad_rebotados)) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'CTR' 
, CASE WHEN rr.cantidad_aperturas > 0 and rr.cantidad_clicks > 0 THEN CAST(((rr.cantidad_clicks*100)/rr.cantidad_aperturas) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'CTOR' 
--, CASE WHEN rr.cantidad_aperturas_U > 0 and rr.cantidad_clicks > 0 THEN CAST(((rr.cantidad_clicks*100)/rr.cantidad_aperturas_U) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'CTUOR' 
, CASE WHEN (rr.cantidad_enviados - rr.cantidad_rebotados) > 0 and rr.cantidad_viral > 0 THEN CAST(((rr.cantidad_viral*100)/(rr.cantidad_enviados - rr.cantidad_rebotados)) AS VARCHAR(5)) ELSE '0' END  + '%' AS 'VR' 
 



	from emblue3.dbo.[_reporte_resumen] rr -- ON datos.reporte_resumen_id = rr.reporte_resumen_id  
	INNER JOIN  emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id  
	LEFT JOIN	emblue3.dbo.[_elementos_envio] en ON en.campanias_elementos_id = ce.campania_elementos_id 
	INNER JOIN  emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id  
	INNER JOIN  emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id 
	INNER JOIN  emblue3.dbo.[_empresa] e ON c.emp_id = e.empresa_id  
	WHERE e.empresa_id=3073
	and DATEDIFF(MONTH,rr.fecha,'2019-01-01') < 3 and DATEDIFF(MONTH,rr.fecha,'2019-01-01') > 0



--select DATEDIFF(MONTH,fecha,'2019-01-01') < 3 and DATEDIFF(MONTH,fecha,'2019-01-01') > 0