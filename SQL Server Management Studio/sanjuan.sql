-- 5084

SELECT 'EMAIL', 
       'FECHA_ENVIO', 
       'FECHA_ACTIVIDAD', 
       'CAMPANIA', 
       'ACCION', 
       'TIPO_ACCION', 
       'ACTIVIDAD', 
       'DESCRIPCION', 
       'TAG', 
       'IDCIMA', 
       'IDCIMB', 
       'IDCIMC' 
UNION ALL 
SELECT e.email                                                                AS 
       'EMAIL', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, rr.fecha), 121)    AS 
       'FECHA_ENVIO', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, datos.fecha), 121) AS 
       'FECHA_ACTIVIDAD', 
       c.nombre                                                               AS 
       'CAMPANA', 
       ce.nombre                                                              AS 
       'ACCION', 
       t.descripcion                                                          AS 
       'TIPO_ACCION', 
       Isnull(datos.actividad, '')                                            AS 
       ACTIVIDAD, 
       Isnull(datos.descripcion, '')                                          AS 
       DESCRIPCION, 
       Isnull(datos.tag, '')                                                  AS 
       TAG, 
       Cast(Replace (cper.campo_27, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMA', 
       Cast(Replace (cper.campo_28, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMB', 
       Cast(Replace (cper.campo_29, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMC' 
FROM   (SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Enviado' AS ACTIVIDAD, 
               ''        AS DESCRIPCION, 
               ''        AS TAG 
        FROM   emblue3.dbo.[5084_reporte_enviado] re 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Abierto'                            AS ACTIVIDAD, 
               Isnull(d.dispositivo, 'Desconocido') AS DESCRIPCION, 
               ''                                   AS TAG 
        FROM   emblue3.dbo.[5084_reporte_abiertos] re 
               LEFT JOIN emblue3.dbo.[_dispositivo] d 
                      ON re.dispositivo_id = d.dispositivo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT links.reporte_resumen_id, 
               links.email_id, 
               links.fecha, 
               links.tipo AS ACTIVIDAD, 
               links.descripcion, 
               tags.tags  AS TAG 
        FROM   (SELECT l.link_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Click'                  AS TIPO, 
                       l.nombre + ' | ' + l.url AS DESCRIPCION 
                FROM   emblue3.dbo.[5084_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[_link] l 
                               ON re.link_id = l.link_id 
                                  AND re.click_tipo_id = 1 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                UNION ALL 
                SELECT s.stuff_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Item'   AS TIPO, 
                       s.titulo AS DESCRIPCION 
                FROM   emblue3.dbo.[5084_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[5084_stuff] s 
                               ON re.link_id = s.stuff_id 
                                  AND re.click_tipo_id = 2 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) links 
               LEFT JOIN (SELECT re.link_id, 
                                 Stuff((SELECT '|' + t.nombre 
                                        FROM   (SELECT l.link_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5084_reporte_clicks] 
                                       re 
                                       INNER JOIN emblue3.dbo.[_link] l 
                                               ON re.link_id = l.link_id 
                                                  AND re.click_tipo_id = 
                                                      1 
                                       INNER JOIN 
                                       emblue3.dbo.[_link_relacion_tag] 
                                       lrt 
                                               ON 
                                       lrt.link_id = l.link_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0 
                                                UNION ALL 
                                                SELECT s.stuff_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5084_reporte_clicks] 
                                       re 
                                       INNER JOIN 
                                       emblue3.dbo.[5084_stuff] s 
                                               ON re.link_id = 
                                                  s.stuff_id 
                                                  AND re.click_tipo_id = 
                                                      2 
                                       INNER JOIN 
                                       emblue3.dbo.[5084_stuff_tag] 
                                       lrt 
                                               ON 
                                       lrt.stuff_id = s.stuff_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0) 
                                               links 
                                               LEFT JOIN emblue3.dbo.[_tag] t 
                                                      ON t.tag_id = links.tag_id 
                                        WHERE  links.link_id = re.link_id 
                                        GROUP  BY t.nombre 
                                        FOR xml path (''), 
                                 type).value('.', 'VARCHAR(255)' ), 1 
               , 1, '') 
                         AS tags 
                          FROM   emblue3.dbo.[5084_reporte_clicks] re 
                          WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                          GROUP  BY re.link_id) tags 
                      ON links.link_id = tags.link_id 
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Desuscripto'  AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5084_reporte_desuscripto] re 
               INNER JOIN emblue3.dbo.[_desuscripcion_tipo] dt 
                       ON re.desuscripcion_tipo_id = dt.desuscripcion_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Rebote'                 AS ACTIVIDAD, 
               CONVERT(VARCHAR, Rtrim(dt.codigo), 100) 
               + ' - ' + dt.descripcion AS DESCRIPCION, 
               ''                       AS TAG 
        FROM   emblue3.dbo.[5084_reporte_rebote] re 
               INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt 
                       ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Viral'        AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5084_reporte_viral] re 
               INNER JOIN emblue3.dbo.[_viral_tipo] dt 
                       ON re.viral_tipo_id = dt.viral_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) AS datos 
       INNER JOIN emblue3.dbo.[_reporte_resumen] rr 
               ON datos.reporte_resumen_id = rr.reporte_resumen_id 
       INNER JOIN emblue3.dbo.[_campania_elementos] ce 
               ON rr.campania_elementos_id = ce.campania_elementos_id 
       INNER JOIN emblue3.dbo.[_elementos_tipos] t 
               ON t.elementos_tipos_id = ce.elementos_tipo_id 
       INNER JOIN emblue3.dbo.[_campania] c 
               ON c.campania_id = ce.campania_id 
       INNER JOIN emblue3.dbo.[_empresa] emp 
               ON c.emp_id = emp.empresa_id 
       INNER JOIN emblue3.dbo.[5084_email_mundo] e 
               ON datos.email_id = e.email_id 
       INNER JOIN emblue3.dbo.[5084_contactos_campos_personalizados_chico] cper 
               ON e.email_id = cper.email_id 
WHERE  emp.empresa_id = 5084 ;


---5085



SELECT 'EMAIL', 
       'FECHA_ENVIO', 
       'FECHA_ACTIVIDAD', 
       'CAMPANIA', 
       'ACCION', 
       'TIPO_ACCION', 
       'ACTIVIDAD', 
       'DESCRIPCION', 
       'TAG', 
       'IDCIMA', 
       'IDCIMB', 
       'IDCIMC' 
UNION ALL 
SELECT e.email                                                                AS 
       'EMAIL', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, rr.fecha), 121)    AS 
       'FECHA_ENVIO', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, datos.fecha), 121) AS 
       'FECHA_ACTIVIDAD', 
       c.nombre                                                               AS 
       'CAMPANA', 
       ce.nombre                                                              AS 
       'ACCION', 
       t.descripcion                                                          AS 
       'TIPO_ACCION', 
       Isnull(datos.actividad, '')                                            AS 
       ACTIVIDAD, 
       Isnull(datos.descripcion, '')                                          AS 
       DESCRIPCION, 
       Isnull(datos.tag, '')                                                  AS 
       TAG, 
       Cast(Replace (cper.campo_26, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMA', 
       Cast(Replace (cper.campo_27, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMB', 
       Cast(Replace (cper.campo_28, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMC' 
FROM   (SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Enviado' AS ACTIVIDAD, 
               ''        AS DESCRIPCION, 
               ''        AS TAG 
        FROM   emblue3.dbo.[5085_reporte_enviado] re 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Abierto'                            AS ACTIVIDAD, 
               Isnull(d.dispositivo, 'Desconocido') AS DESCRIPCION, 
               ''                                   AS TAG 
        FROM   emblue3.dbo.[5085_reporte_abiertos] re 
               LEFT JOIN emblue3.dbo.[_dispositivo] d 
                      ON re.dispositivo_id = d.dispositivo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT links.reporte_resumen_id, 
               links.email_id, 
               links.fecha, 
               links.tipo AS ACTIVIDAD, 
               links.descripcion, 
               tags.tags  AS TAG 
        FROM   (SELECT l.link_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Click'                  AS TIPO, 
                       l.nombre + ' | ' + l.url AS DESCRIPCION 
                FROM   emblue3.dbo.[5085_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[_link] l 
                               ON re.link_id = l.link_id 
                                  AND re.click_tipo_id = 1 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                UNION ALL 
                SELECT s.stuff_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Item'   AS TIPO, 
                       s.titulo AS DESCRIPCION 
                FROM   emblue3.dbo.[5085_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[5085_stuff] s 
                               ON re.link_id = s.stuff_id 
                                  AND re.click_tipo_id = 2 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) links 
               LEFT JOIN (SELECT re.link_id, 
                                 Stuff((SELECT '|' + t.nombre 
                                        FROM   (SELECT l.link_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5085_reporte_clicks] 
                                       re 
                                       INNER JOIN emblue3.dbo.[_link] l 
                                               ON re.link_id = l.link_id 
                                                  AND re.click_tipo_id = 
                                                      1 
                                       INNER JOIN 
                                       emblue3.dbo.[_link_relacion_tag] 
                                       lrt 
                                               ON 
                                       lrt.link_id = l.link_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0 
                                                UNION ALL 
                                                SELECT s.stuff_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5085_reporte_clicks] 
                                       re 
                                       INNER JOIN 
                                       emblue3.dbo.[5085_stuff] s 
                                               ON re.link_id = 
                                                  s.stuff_id 
                                                  AND re.click_tipo_id = 
                                                      2 
                                       INNER JOIN 
                                       emblue3.dbo.[5085_stuff_tag] 
                                       lrt 
                                               ON 
                                       lrt.stuff_id = s.stuff_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0) 
                                               links 
                                               LEFT JOIN emblue3.dbo.[_tag] t 
                                                      ON t.tag_id = links.tag_id 
                                        WHERE  links.link_id = re.link_id 
                                        GROUP  BY t.nombre 
                                        FOR xml path (''), 
                                 type).value('.', 'VARCHAR(255)' ), 1 
               , 1, '') 
                         AS tags 
                          FROM   emblue3.dbo.[5085_reporte_clicks] re 
                          WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                          GROUP  BY re.link_id) tags 
                      ON links.link_id = tags.link_id 
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Desuscripto'  AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5085_reporte_desuscripto] re 
               INNER JOIN emblue3.dbo.[_desuscripcion_tipo] dt 
                       ON re.desuscripcion_tipo_id = dt.desuscripcion_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Rebote'                 AS ACTIVIDAD, 
               CONVERT(VARCHAR, Rtrim(dt.codigo), 100) 
               + ' - ' + dt.descripcion AS DESCRIPCION, 
               ''                       AS TAG 
        FROM   emblue3.dbo.[5085_reporte_rebote] re 
               INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt 
                       ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Viral'        AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5085_reporte_viral] re 
               INNER JOIN emblue3.dbo.[_viral_tipo] dt 
                       ON re.viral_tipo_id = dt.viral_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) AS datos 
       INNER JOIN emblue3.dbo.[_reporte_resumen] rr 
               ON datos.reporte_resumen_id = rr.reporte_resumen_id 
       INNER JOIN emblue3.dbo.[_campania_elementos] ce 
               ON rr.campania_elementos_id = ce.campania_elementos_id 
       INNER JOIN emblue3.dbo.[_elementos_tipos] t 
               ON t.elementos_tipos_id = ce.elementos_tipo_id 
       INNER JOIN emblue3.dbo.[_campania] c 
               ON c.campania_id = ce.campania_id 
       INNER JOIN emblue3.dbo.[_empresa] emp 
               ON c.emp_id = emp.empresa_id 
       INNER JOIN emblue3.dbo.[5085_email_mundo] e 
               ON datos.email_id = e.email_id 
       INNER JOIN emblue3.dbo.[5085_contactos_campos_personalizados_chico] cper 
               ON e.email_id = cper.email_id 
WHERE  emp.empresa_id = 5085 



-----5083

SELECT 'EMAIL', 
       'FECHA_ENVIO', 
       'FECHA_ACTIVIDAD', 
       'CAMPANIA', 
       'ACCION', 
       'TIPO_ACCION', 
       'ACTIVIDAD', 
       'DESCRIPCION', 
       'TAG', 
       'IDCIMA', 
       'IDCIMB', 
       'IDCIMC' 
UNION ALL 
SELECT e.email                                                                AS 
       'EMAIL', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, rr.fecha), 121)    AS 
       'FECHA_ENVIO', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, datos.fecha), 121) AS 
       'FECHA_ACTIVIDAD', 
       c.nombre                                                               AS 
       'CAMPANA', 
       ce.nombre                                                              AS 
       'ACCION', 
       t.descripcion                                                          AS 
       'TIPO_ACCION', 
       Isnull(datos.actividad, '')                                            AS 
       ACTIVIDAD, 
       Isnull(datos.descripcion, '')                                          AS 
       DESCRIPCION, 
       Isnull(datos.tag, '')                                                  AS 
       TAG, 
       Cast(Replace (cper.campo_37, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMA', 
       Cast(Replace (cper.campo_38, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMB', 
       Cast(Replace (cper.campo_39, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMC' 
FROM   (SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Enviado' AS ACTIVIDAD, 
               ''        AS DESCRIPCION, 
               ''        AS TAG 
        FROM   emblue3.dbo.[5083_reporte_enviado] re 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Abierto'                            AS ACTIVIDAD, 
               Isnull(d.dispositivo, 'Desconocido') AS DESCRIPCION, 
               ''                                   AS TAG 
        FROM   emblue3.dbo.[5083_reporte_abiertos] re 
               LEFT JOIN emblue3.dbo.[_dispositivo] d 
                      ON re.dispositivo_id = d.dispositivo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT links.reporte_resumen_id, 
               links.email_id, 
               links.fecha, 
               links.tipo AS ACTIVIDAD, 
               links.descripcion, 
               tags.tags  AS TAG 
        FROM   (SELECT l.link_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Click'                  AS TIPO, 
                       l.nombre + ' | ' + l.url AS DESCRIPCION 
                FROM   emblue3.dbo.[5083_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[_link] l 
                               ON re.link_id = l.link_id 
                                  AND re.click_tipo_id = 1 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                UNION ALL 
                SELECT s.stuff_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Item'   AS TIPO, 
                       s.titulo AS DESCRIPCION 
                FROM   emblue3.dbo.[5083_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[5083_stuff] s 
                               ON re.link_id = s.stuff_id 
                                  AND re.click_tipo_id = 2 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) links 
               LEFT JOIN (SELECT re.link_id, 
                                 Stuff((SELECT '|' + t.nombre 
                                        FROM   (SELECT l.link_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5083_reporte_clicks] 
                                       re 
                                       INNER JOIN emblue3.dbo.[_link] l 
                                               ON re.link_id = l.link_id 
                                                  AND re.click_tipo_id = 
                                                      1 
                                       INNER JOIN 
                                       emblue3.dbo.[_link_relacion_tag] 
                                       lrt 
                                               ON 
                                       lrt.link_id = l.link_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0 
                                                UNION ALL 
                                                SELECT s.stuff_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5083_reporte_clicks] 
                                       re 
                                       INNER JOIN 
                                       emblue3.dbo.[5083_stuff] s 
                                               ON re.link_id = 
                                                  s.stuff_id 
                                                  AND re.click_tipo_id = 
                                                      2 
                                       INNER JOIN 
                                       emblue3.dbo.[5083_stuff_tag] 
                                       lrt 
                                               ON 
                                       lrt.stuff_id = s.stuff_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0) 
                                               links 
                                               LEFT JOIN emblue3.dbo.[_tag] t 
                                                      ON t.tag_id = links.tag_id 
                                        WHERE  links.link_id = re.link_id 
                                        GROUP  BY t.nombre 
                                        FOR xml path (''), 
                                 type).value('.', 'VARCHAR(255)' ), 1 
               , 1, '') 
                         AS tags 
                          FROM   emblue3.dbo.[5083_reporte_clicks] re 
                          WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                          GROUP  BY re.link_id) tags 
                      ON links.link_id = tags.link_id 
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Desuscripto'  AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5083_reporte_desuscripto] re 
               INNER JOIN emblue3.dbo.[_desuscripcion_tipo] dt 
                       ON re.desuscripcion_tipo_id = dt.desuscripcion_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Rebote'                 AS ACTIVIDAD, 
               CONVERT(VARCHAR, Rtrim(dt.codigo), 100) 
               + ' - ' + dt.descripcion AS DESCRIPCION, 
               ''                       AS TAG 
        FROM   emblue3.dbo.[5083_reporte_rebote] re 
               INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt 
                       ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Viral'        AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5083_reporte_viral] re 
               INNER JOIN emblue3.dbo.[_viral_tipo] dt 
                       ON re.viral_tipo_id = dt.viral_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) AS datos 
       INNER JOIN emblue3.dbo.[_reporte_resumen] rr 
               ON datos.reporte_resumen_id = rr.reporte_resumen_id 
       INNER JOIN emblue3.dbo.[_campania_elementos] ce 
               ON rr.campania_elementos_id = ce.campania_elementos_id 
       INNER JOIN emblue3.dbo.[_elementos_tipos] t 
               ON t.elementos_tipos_id = ce.elementos_tipo_id 
       INNER JOIN emblue3.dbo.[_campania] c 
               ON c.campania_id = ce.campania_id 
       INNER JOIN emblue3.dbo.[_empresa] emp 
               ON c.emp_id = emp.empresa_id 
       INNER JOIN emblue3.dbo.[5083_email_mundo] e 
               ON datos.email_id = e.email_id 
       INNER JOIN emblue3.dbo.[5083_contactos_campos_personalizados_chico] cper 
               ON e.email_id = cper.email_id 
WHERE  emp.empresa_id = 5083 



----5082



SELECT 'EMAIL', 
       'FECHA_ENVIO', 
       'FECHA_ACTIVIDAD', 
       'CAMPANIA', 
       'ACCION', 
       'TIPO_ACCION', 
       'ACTIVIDAD', 
       'DESCRIPCION', 
       'TAG', 
       'IDCIMA', 
       'IDCIMB', 
       'IDCIMC' 
UNION ALL 
SELECT e.email                                                                AS 
       'EMAIL', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, rr.fecha), 121)    AS 
       'FECHA_ENVIO', 
       CONVERT(VARCHAR(24), Dateadd(hour, emp.time_offset, datos.fecha), 121) AS 
       'FECHA_ACTIVIDAD', 
       c.nombre                                                               AS 
       'CAMPANA', 
       ce.nombre                                                              AS 
       'ACCION', 
       t.descripcion                                                          AS 
       'TIPO_ACCION', 
       Isnull(datos.actividad, '')                                            AS 
       ACTIVIDAD, 
       Isnull(datos.descripcion, '')                                          AS 
       DESCRIPCION, 
       Isnull(datos.tag, '')                                                  AS 
       TAG, 
       Cast(Replace (cper.campo_27, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMA', 
       Cast(Replace (cper.campo_28, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMB', 
       Cast(Replace (cper.campo_29, ';', ',') AS VARCHAR(max))                AS 
       'IDCIMC' 
FROM   (SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Enviado' AS ACTIVIDAD, 
               ''        AS DESCRIPCION, 
               ''        AS TAG 
        FROM   emblue3.dbo.[5082_reporte_enviado] re 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Abierto'                            AS ACTIVIDAD, 
               Isnull(d.dispositivo, 'Desconocido') AS DESCRIPCION, 
               ''                                   AS TAG 
        FROM   emblue3.dbo.[5082_reporte_abiertos] re 
               LEFT JOIN emblue3.dbo.[_dispositivo] d 
                      ON re.dispositivo_id = d.dispositivo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT links.reporte_resumen_id, 
               links.email_id, 
               links.fecha, 
               links.tipo AS ACTIVIDAD, 
               links.descripcion, 
               tags.tags  AS TAG 
        FROM   (SELECT l.link_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Click'                  AS TIPO, 
                       l.nombre + ' | ' + l.url AS DESCRIPCION 
                FROM   emblue3.dbo.[5082_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[_link] l 
                               ON re.link_id = l.link_id 
                                  AND re.click_tipo_id = 1 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                UNION ALL 
                SELECT s.stuff_id, 
                       re.reporte_resumen_id, 
                       re.email_id, 
                       re.fecha, 
                       'Item'   AS TIPO, 
                       s.titulo AS DESCRIPCION 
                FROM   emblue3.dbo.[5082_reporte_clicks] re 
                       INNER JOIN emblue3.dbo.[5082_stuff] s 
                               ON re.link_id = s.stuff_id 
                                  AND re.click_tipo_id = 2 
                WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) links 
               LEFT JOIN (SELECT re.link_id, 
                                 Stuff((SELECT '|' + t.nombre 
                                        FROM   (SELECT l.link_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5082_reporte_clicks] 
                                       re 
                                       INNER JOIN emblue3.dbo.[_link] l 
                                               ON re.link_id = l.link_id 
                                                  AND re.click_tipo_id = 
                                                      1 
                                       INNER JOIN 
                                       emblue3.dbo.[_link_relacion_tag] 
                                       lrt 
                                               ON 
                                       lrt.link_id = l.link_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0 
                                                UNION ALL 
                                                SELECT s.stuff_id, 
                                                       lrt.tag_id 
                                                FROM 
                                       emblue3.dbo.[5082_reporte_clicks] 
                                       re 
                                       INNER JOIN 
                                       emblue3.dbo.[5082_stuff] s 
                                               ON re.link_id = 
                                                  s.stuff_id 
                                                  AND re.click_tipo_id = 
                                                      2 
                                       INNER JOIN 
                                       emblue3.dbo.[5082_stuff_tag] 
                                       lrt 
                                               ON 
                                       lrt.stuff_id = s.stuff_id 
                                                WHERE 
                                       Datediff(day, re.fecha, Getutcdate() - 1) 
                                       = 0) 
                                               links 
                                               LEFT JOIN emblue3.dbo.[_tag] t 
                                                      ON t.tag_id = links.tag_id 
                                        WHERE  links.link_id = re.link_id 
                                        GROUP  BY t.nombre 
                                        FOR xml path (''), 
                                 type).value('.', 'VARCHAR(255)' ), 1 
               , 1, '') 
                         AS tags 
                          FROM   emblue3.dbo.[5082_reporte_clicks] re 
                          WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
                          GROUP  BY re.link_id) tags 
                      ON links.link_id = tags.link_id 
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Desuscripto'  AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5082_reporte_desuscripto] re 
               INNER JOIN emblue3.dbo.[_desuscripcion_tipo] dt 
                       ON re.desuscripcion_tipo_id = dt.desuscripcion_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Rebote'                 AS ACTIVIDAD, 
               CONVERT(VARCHAR, Rtrim(dt.codigo), 100) 
               + ' - ' + dt.descripcion AS DESCRIPCION, 
               ''                       AS TAG 
        FROM   emblue3.dbo.[5082_reporte_rebote] re 
               INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt 
                       ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0
        UNION ALL 
        SELECT re.reporte_resumen_id, 
               re.email_id, 
               re.fecha, 
               'Viral'        AS ACTIVIDAD, 
               dt.descripcion AS DESCRIPCION, 
               ''             AS TAG 
        FROM   emblue3.dbo.[5082_reporte_viral] re 
               INNER JOIN emblue3.dbo.[_viral_tipo] dt 
                       ON re.viral_tipo_id = dt.viral_tipo_id 
        WHERE  Datediff(day, re.fecha, '2020-02-21') = 0) AS datos 
       INNER JOIN emblue3.dbo.[_reporte_resumen] rr 
               ON datos.reporte_resumen_id = rr.reporte_resumen_id 
       INNER JOIN emblue3.dbo.[_campania_elementos] ce 
               ON rr.campania_elementos_id = ce.campania_elementos_id 
       INNER JOIN emblue3.dbo.[_elementos_tipos] t 
               ON t.elementos_tipos_id = ce.elementos_tipo_id 
       INNER JOIN emblue3.dbo.[_campania] c 
               ON c.campania_id = ce.campania_id 
       INNER JOIN emblue3.dbo.[_empresa] emp 
               ON c.emp_id = emp.empresa_id 
       INNER JOIN emblue3.dbo.[5082_email_mundo] e 
               ON datos.email_id = e.email_id 
       INNER JOIN emblue3.dbo.[5082_contactos_campos_personalizados_chico] cper 
               ON e.email_id = cper.email_id 
WHERE  emp.empresa_id = 5082 