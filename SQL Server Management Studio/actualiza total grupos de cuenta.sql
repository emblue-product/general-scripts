

BEGIN
          DECLARE @consultaSQL VARCHAR(max) 
		  DECLARE @grupo_id VARCHAR(40)

		  DECLARE gruposylistas_id CURSOR FOR

				  select gruposylistas_id 
				  from _gruposylistas 
				  where emp_id = 1465
						and fecha_borrado is null

		  OPEN gruposylistas_id 

          FETCH next FROM gruposylistas_id INTO @grupo_id

		WHILE @@FETCH_STATUS = 0 
            BEGIN 

				SET @consultaSQL = 'UPDATE g SET    g.rank = Isnull(recalcular.ranking, 0) /  Isnull(NULLIF(recalcular.contactos, 0), 1), 
										g.cantidad_rank = Isnull(recalcular.ranking, 0), 
										g.cantidad_contactos = Isnull(recalcular.contactos, 0) 

						FROM   _gruposylistas g 
							   INNER JOIN (
											   SELECT ' + @grupo_id + '      AS grupoID, 
														Sum(cp.rank)       AS ranking, 
														Count(cp.email_id) AS contactos 
												FROM   [1465_contactos_gruposylistas] gyl 
													INNER JOIN [1465_contactos_propiedades] cp  ON gyl.gruposylistas_id = ' + @grupo_id + '  AND gyl.email_id = cp.email_id
											)
										 AS recalcular ON g.gruposylistas_id = recalcular.grupoid '
				PRINT @consultaSQL
				EXEC(@consultaSQL) 
				FETCH next FROM gruposylistas_id INTO @grupo_id
            END 
		CLOSE gruposylistas_id 
        DEALLOCATE gruposylistas_id 
END