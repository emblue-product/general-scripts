--- se debe sacar permisos de administrador
--- 1) asignar empresas a usuarios
Declare @contratoId int, @usuario_id int

set @contratoId = 28109	--contrato asignado al farmer
set @usuario_id = 37630 --usuario del farmer

INSERT INTO [_nodo_seguridad_usuario]

SELECT @usuario_id AS 'USUARIO_ID', nodo_seguridad_id AS 'NODO_SEGURIDAD_ID', '5' AS 'ROL_USUARIO_ID'
FROM [_nodo_seguridad]
WHERE nodo_agencia_id = (
							SELECT nodo_seguridad_id 
							FROM [_nodo_seguridad]
							WHERE agencia_id = (
													SELECT agencia_id FROM  [_agencia] WHERE contrato = @contratoId
												)
						) 
						
	and nodo_seguridad_id not in (
									select nodo_seguridad_id from [_nodo_seguridad_usuario] where  usuario_id = @usuario_id
								)

-- 2) saca permisos a usuarios

IF NOT EXISTS (SELECT * FROM [_funcion_emblue_usuario_denegada] WHERE usuario_id = @usuario_id) 
	BEGIN
	   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (1 ,@usuario_id) 
	   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (2 ,@usuario_id)
	   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (3 ,@usuario_id)
	   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (4 ,@usuario_id)
	END
ELSE
begin
	IF NOT EXISTS (SELECT * FROM [_funcion_emblue_usuario_denegada] WHERE funcion_emblue_id = 1 AND usuario_id = @usuario_id) 
		BEGIN
			print('Se agrega restriccion 1')
		   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (1 ,@usuario_id)
		END
	else
		print('tiene 1')
	IF NOT EXISTS (SELECT * FROM [_funcion_emblue_usuario_denegada] WHERE funcion_emblue_id = 2 AND usuario_id = @usuario_id) 
		BEGIN
			print('Se agrega restriccion 2')
		   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (2 ,@usuario_id)
		END
	else
		print('tiene 2')
	IF NOT EXISTS (SELECT * FROM [_funcion_emblue_usuario_denegada] WHERE funcion_emblue_id = 3 AND usuario_id = @usuario_id) 
		BEGIN
			print('Se agrega restriccion 3')
		   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (3 ,@usuario_id)
		END
	else
		print('tiene 3')
	IF NOT EXISTS (SELECT * FROM [_funcion_emblue_usuario_denegada] WHERE funcion_emblue_id = 4 AND usuario_id = @usuario_id) 
		BEGIN
			print('Se agrega restriccion 4')
		   INSERT INTO [_funcion_emblue_usuario_denegada]     VALUES           (4 ,@usuario_id)
		END
	else
		print('tiene 4')
END



