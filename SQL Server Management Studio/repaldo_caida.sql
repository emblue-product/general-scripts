
--------------------------- detalle diario
SELECT e.email as 'EMAIL'
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO'
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD'
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD, ISNULL(datos.DESCRIPCION,'') AS DESCRIPCION, ISNULL(datos.TAG,'') AS TAG,   cper.campo_26 as 'ENCRIPTADO' --, cper.ID_CONTACTO as 'ID_CONTACTO', cper.PAIS as 'PAIS' 
FROM ( 
	SELECT  re.reporte_resumen_id ,re.email_id, re.fecha, 'Enviado' AS ACTIVIDAD, '' AS DESCRIPCION, '' AS TAG   
	FROM emblue3.dbo.[8263_reporte_enviado] re 
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0 
	UNION ALL 
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Abierto' AS ACTIVIDAD, ISNULL(d.dispositivo,'Desconocido') AS DESCRIPCION, '' AS TAG  
	FROM  emblue3.dbo.[8263_reporte_abiertos] re LEFT JOIN emblue3.dbo.[_dispositivo] d ON re.dispositivo_id = d.dispositivo_id   
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0 	
	UNION ALL 
	SELECT  links.reporte_resumen_id,links.email_id, links.fecha, links.TIPO  AS ACTIVIDAD, links.DESCRIPCION, tags.tags  AS TAG  
	FROM ( 
		SELECT l.link_id, re.reporte_resumen_id, re.email_id, re.fecha, 'Click' AS TIPO, l.nombre + ' | ' + l.url AS DESCRIPCION 
		FROM emblue3.dbo.[8263_reporte_clicks] re 
		INNER JOIN emblue3.dbo.[_link] l ON re.link_id = l.link_id AND re.click_tipo_id = 1  
		WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0 
		UNION ALL 
		SELECT s.stuff_id, re.reporte_resumen_id, re.email_id, re.fecha, 'Item' AS TIPO, s.titulo AS DESCRIPCION 
		FROM emblue3.dbo.[8263_reporte_clicks] re 
		INNER JOIN emblue3.dbo.[8263_stuff] s ON re.link_id = s.stuff_id AND re.click_tipo_id = 2  
		WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0 
	) links 
	LEFT JOIN ( 
		SELECT re.link_id, STUFF( 
		( 
			SELECT '|' + t.nombre 
			FROM ( 
				SELECT l.link_id, lrt.tag_id 
				FROM emblue3.dbo.[8263_reporte_clicks] re  
				INNER JOIN emblue3.dbo.[_link] l ON re.link_id = l.link_id AND re.click_tipo_id = 1   
				INNER JOIN emblue3.dbo.[_link_relacion_tag] lrt ON lrt.link_id=l.link_id 
				WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0  
				UNION ALL 
				SELECT s.stuff_id, lrt.tag_id  
				FROM emblue3.dbo.[8263_reporte_clicks] re  
				INNER JOIN emblue3.dbo.[8263_stuff] s ON re.link_id = s.stuff_id AND re.click_tipo_id = 2   
				INNER JOIN emblue3.dbo.[8263_stuff_tag] lrt ON lrt.stuff_id=s.stuff_id 
				WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0  
			) links 
			LEFT JOIN emblue3.dbo.[_tag] t ON t.tag_id=links.tag_id			 
			WHERE links.link_id=re.link_id 
			GROUP BY t.nombre 
			FOR XML PATH (''), TYPE).value('.','VARCHAR(255)'  
			), 1, 1, ''  
		)  AS tags 
		FROM emblue3.dbo.[8263_reporte_clicks] re  
		WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0  
		GROUP BY re.link_id 
	) tags ON links.link_id = tags.link_id 
	UNION ALL      
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Desuscripto' AS ACTIVIDAD, dt.descripcion AS DESCRIPCION, '' AS TAG   
	FROM emblue3.dbo.[8263_reporte_desuscripto] re 
	INNER JOIN emblue3.dbo.[_desuscripcion_tipo] dt ON re.desuscripcion_tipo_id = dt.desuscripcion_tipo_id    
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0  	
	UNION ALL     
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Rebote' AS ACTIVIDAD, convert(varchar,RTRIM(dt.codigo),100) + ' - ' + dt.descripcion AS DESCRIPCION, '' AS TAG   
	FROM emblue3.dbo.[8263_reporte_rebote] re INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id   
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0   
	UNION ALL     
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Viral' AS ACTIVIDAD, dt.descripcion  AS DESCRIPCION, '' AS TAG   
	FROM emblue3.dbo.[8263_reporte_viral] re 
	INNER JOIN emblue3.dbo.[_viral_tipo] dt ON re.viral_tipo_id = dt.viral_tipo_id    
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0 
) AS datos 
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id     
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id    
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id   
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id  
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id  
INNER JOIN emblue3.dbo.[8263_email_mundo] e  ON datos.email_id = e.email_id  
INNER JOIN emblue3.dbo.[8263_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id 
WHERE emp.empresa_id=8263  


----- actividad enviados diarios

SELECT e.email as 'EMAIL' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD' 
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD,  cper.telefono_1 as 'TELEFONO_1', cper.campo_26 as 'SEGMENTO', cper.campo_27 as 'CODCAMPANA'  
FROM (
	SELECT  re.reporte_resumen_id ,re.email_id, re.fecha, 'Enviado' AS ACTIVIDAD, '' AS DESCRIPCION, '' AS TAG   
	FROM emblue3.dbo.[8263_reporte_enviado] re  
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0 
) AS datos  
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id     
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id     
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id    
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id   
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id  
INNER JOIN emblue3.dbo.[8263_email_mundo] e  ON datos.email_id = e.email_id  
INNER JOIN emblue3.dbo.[8263_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id  
WHERE emp.empresa_id=8263  

------ REBOTES DIARIOS

SELECT e.email as 'EMAIL' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD' 
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD, ISNULL(datos.DESCRIPCION,'') AS DESCRIPCION,  cper.telefono_1 as 'TELEFONO_1', cper.campo_26 as 'SEGMENTO', cper.campo_27 as 'CODCAMPANA' 
FROM ( 
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Rebote' AS ACTIVIDAD, convert(varchar,RTRIM(dt.codigo),100) + ' - ' + dt.descripcion AS DESCRIPCION     
	FROM emblue3.dbo.[8263_reporte_rebote] re INNER JOIN emblue3.dbo.[_rebote_codigo_error] dt ON re.rebote_codigo_error_id = dt.rebote_codigo_error_id    
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0
) AS datos   
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id     
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id    
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id   
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id   
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id  
INNER JOIN emblue3.dbo.[8263_email_mundo] e  ON datos.email_id = e.email_id  
INNER JOIN emblue3.dbo.[8263_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id  
WHERE emp.empresa_id=8263  


----- actividad log tag diarios

SELECT datos.EMAIL, datos.FECHA_ACTIVIDAD, datos.TIPO, datos.TAG, datos.TAG_ID, datos.FUENTE 
, ISNULL(c.nombre,'') AS CAMPANIA, ISNULL(CAST(c.campania_id AS VARCHAR(MAX)),'') AS CAMPANIA_ID 
, ISNULL(ce.nombre,'') AS ACCION, ISNULL(CAST(ce.campania_elementos_id AS VARCHAR(MAX)),'') AS ACCION_ID 
, ISNULL(datosLink.ACTIVIDAD, ISNULL(datosRemarketing.ACTIVIDAD, ISNULL(datosWebTracking.ACTIVIDAD,''))) AS ACTIVIDAD 
, ISNULL(datosLink.DESCRIPCION, ISNULL(datosRemarketing.DESCRIPCION, ISNULL(datosWebTracking.DESCRIPCION,''))) AS DESCRIPCION   
FROM (  
	SELECT e.email AS EMAIL  
	, CONVERT(VARCHAR(24), CONVERT(DATETIME, tgl.fecha, 103), 121)  AS FECHA_ACTIVIDAD   
	, tta.descripcion AS TIPO   
	, t.nombre AS TAG  
	, ISNULL(CAST(t.tag_id AS VARCHAR(MAX)),'0') AS TAG_ID  
	, tf.fuente AS FUENTE  
	, tgl.tag_fuente_id, tgl.referencia_id  
	FROM emblue3.dbo.[_tag] t   
	INNER JOIN	emblue3.dbo.[_tag_relacion_tag_log] trtl ON t.tag_id=trtl.tag_id   
	LEFT JOIN	emblue3.dbo.[8263_tag_log] tgl ON trtl.tag_id_log=tgl.tag_id    
	INNER JOIN	emblue3.dbo.[_tag_fuente] tf ON tgl.tag_fuente_id=tf.tag_fuente_id   
	INNER JOIN	emblue3.dbo.[8263_email_mundo] e ON e.email_id=tgl.email_id   
	INNER JOIN	emblue3.dbo.[_tag_tipo_actividad] tta  ON tta.tag_tipo_actividad_id=tgl.tag_tipo_actividad   
	LEFT JOIN	emblue3.dbo.[_tag_categoria] tc ON tc.tag_categoria_id = t.tag_categoria_id   
	WHERE t.empresa_id = 8263 AND DATEDIFF(DAY,TGL.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,TGL.fecha,'2019-03-09') > 0   
) datos  
LEFT JOIN ( 
	SELECT l.link_id AS linkId, l.url AS url, l.nombre AS DESCRIPCION, l.elemento_envio_id, 'Click' as ACTIVIDAD  
	FROM emblue3.dbo.[_link] l  
) datosLink ON datosLink.linkId = datos.referencia_id AND datos.tag_fuente_id <> 7 AND datos.tag_fuente_id <> 6   
LEFT JOIN (  
    SELECT ee.elementos_envio_id, rr.reporte_resumen_id, '' as DESCRIPCION, 'WebTracking' AS ACTIVIDAD  
    FROM emblue3.dbo.[_reporte_resumen] rr  
    INNER JOIN emblue3.dbo.[_elementos_envio] ee ON ee.campanias_elementos_id = rr.campania_elementos_id  
) datosWebTracking ON datosWebTracking.reporte_resumen_id = datos.referencia_id AND datos.tag_fuente_id = 6   
LEFT JOIN (  
	SELECT s.stuff_id AS StuffId, s.titulo AS DESCRIPCION, s.elemento_envio_id, 'Item' AS ACTIVIDAD  
	FROM emblue3.dbo.[8263_stuff] s  
) datosRemarketing ON datosRemarketing.StuffId = datos.referencia_id AND datos.tag_fuente_id = 7   
LEFT JOIN emblue3.dbo.[_elementos_envio] ee ON ee.elementos_envio_id = ISNULL(datosLink.elemento_envio_id, ISNULL(datosRemarketing.elemento_envio_id, datosWebTracking.elementos_envio_id))  
LEFT JOIN emblue3.dbo.[_campania_elementos] ce ON ce.campania_elementos_id = ee.campanias_elementos_id  
LEFT JOIN emblue3.dbo.[_campania] c ON ce.campania_id = c.campania_id   
LEFT JOIN emblue3.dbo.[_empresa] e ON e.empresa_id=c.emp_id  

----- aperturas diarias

SELECT e.email as 'EMAIL' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD' 
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD, ISNULL(datos.DESCRIPCION,'') AS DESCRIPCION,   cper.telefono_1 as 'TELEFONO_1', cper.campo_26 as 'SEGMENTO', cper.campo_27 as 'CODCAMPANA' 
FROM ( 
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Abierto' AS ACTIVIDAD, ISNULL(d.dispositivo,'Desconocido') AS DESCRIPCION  
	FROM  emblue3.dbo.[8263_reporte_abiertos] re LEFT JOIN emblue3.dbo.[_dispositivo] d ON re.dispositivo_id = d.dispositivo_id    
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0  
) AS datos  
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id      
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id     
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id   
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id   
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id   
INNER JOIN emblue3.dbo.[8263_email_mundo] e  ON datos.email_id = e.email_id   
INNER JOIN emblue3.dbo.[8263_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id  
WHERE emp.empresa_id=8263  


------------------ DESUSCRITOS DIARIOS

SELECT e.email as 'EMAIL' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD' 
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD, ISNULL(datos.DESCRIPCION,'') AS DESCRIPCION  
FROM ( 
	SELECT  re.reporte_resumen_id, re.email_id, re.fecha, 'Desuscripto' AS ACTIVIDAD, dt.descripcion AS DESCRIPCION    
	FROM emblue3.dbo.[8263_reporte_desuscripto] re  
	INNER JOIN emblue3.dbo.[_desuscripcion_tipo] dt ON re.desuscripcion_tipo_id = dt.desuscripcion_tipo_id     
	WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0    
) AS datos  
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id      
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id     
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id    
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id   
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id   
INNER JOIN emblue3.dbo.[8263_email_mundo] e  ON datos.email_id = e.email_id   
INNER JOIN emblue3.dbo.[8263_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id  
WHERE emp.empresa_id=8263  


---- click diarios
SELECT e.email as 'EMAIL' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,rr.fecha), 121) AS 'FECHA_ENVIO' 
, CONVERT(VARCHAR(24), DATEADD(HOUR,emp.time_offset,datos.fecha), 121) AS 'FECHA_ACTIVIDAD' 
, c.nombre AS 'CAMPANA', ce.nombre AS 'ACCION', t.descripcion AS 'TIPO_ACCION'
, ISNULL(datos.ACTIVIDAD,'') AS ACTIVIDAD, ISNULL(datos.DESCRIPCION,'') AS DESCRIPCION, ISNULL(datos.TAG,'') AS TAG , cper.telefono_1 as 'TELEFONO_1', cper.campo_26 as 'SEGMENTO', cper.campo_27 as 'CODCAMPANA' 
FROM ( 
	SELECT  links.reporte_resumen_id,links.email_id, links.fecha, links.TIPO  AS ACTIVIDAD, links.DESCRIPCION, tags.tags  AS TAG 
	FROM ( 
		SELECT l.link_id, re.reporte_resumen_id, re.email_id, re.fecha, 'Click' AS TIPO, l.nombre + '|' + l.url AS DESCRIPCION 
		FROM emblue3.dbo.[8263_reporte_clicks] re  
		INNER JOIN emblue3.dbo.[_link] l ON re.link_id = l.link_id AND re.click_tipo_id = 1   
		WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0   
		UNION ALL  
		SELECT s.stuff_id, re.reporte_resumen_id, re.email_id, re.fecha, 'Item' AS TIPO, s.titulo AS DESCRIPCION 
		FROM emblue3.dbo.[8263_reporte_clicks] re  
		INNER JOIN emblue3.dbo.[8263_stuff] s ON re.link_id = s.stuff_id AND re.click_tipo_id = 2   
		WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0   
	) links 
	LEFT JOIN ( 
		SELECT re.link_id, STUFF( 
		(  
			SELECT ',' + t.nombre  
			FROM ( 
				SELECT l.link_id, lrt.tag_id 
				FROM emblue3.dbo.[8263_reporte_clicks] re  
				INNER JOIN emblue3.dbo.[_link] l ON re.link_id = l.link_id AND re.click_tipo_id = 1   
				INNER JOIN emblue3.dbo.[_link_relacion_tag] lrt ON lrt.link_id=l.link_id 
				WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0   
				UNION ALL  
				SELECT s.stuff_id, lrt.tag_id  
				FROM emblue3.dbo.[8263_reporte_clicks] re  
				INNER JOIN emblue3.dbo.[8263_stuff] s ON re.link_id = s.stuff_id AND re.click_tipo_id = 2   
				INNER JOIN emblue3.dbo.[8263_stuff_tag] lrt ON lrt.stuff_id=s.stuff_id  
				WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0    
			) links  
			LEFT JOIN emblue3.dbo.[_tag] t ON t.tag_id=links.tag_id			  
			WHERE links.link_id=re.link_id  
			GROUP BY t.nombre  
			FOR XML PATH (''), TYPE).value('.','VARCHAR(255)'  
			), 1, 1, ''   
		)  AS tags  
		FROM emblue3.dbo.[8263_reporte_clicks] re   
		WHERE DATEDIFF(DAY,re.fecha,'2019-03-09') < 9  and DATEDIFF(DAY,re.fecha,'2019-03-09') > 0    
		GROUP BY re.link_id  
	) tags ON links.link_id = tags.link_id  
) AS datos   
INNER JOIN emblue3.dbo.[_reporte_resumen] rr ON datos.reporte_resumen_id = rr.reporte_resumen_id       
INNER JOIN emblue3.dbo.[_campania_elementos] ce ON rr.campania_elementos_id = ce.campania_elementos_id      
INNER JOIN emblue3.dbo.[_elementos_tipos] t ON t.elementos_tipos_id = ce.elementos_tipo_id     
INNER JOIN emblue3.dbo.[_campania] c on c.campania_id = ce.campania_id    
INNER JOIN emblue3.dbo.[_empresa] emp on c.emp_id = emp.empresa_id    
INNER JOIN emblue3.dbo.[8263_email_mundo] e  ON datos.email_id = e.email_id    
INNER JOIN emblue3.dbo.[8263_contactos_campos_personalizados_chico] cper ON e.email_id = cper.email_id   
WHERE emp.empresa_id=8263  
