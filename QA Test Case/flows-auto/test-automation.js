import fetch from 'node-fetch'

const credentials = 'basic ODlhOTZiYjhiZmY3NDIwMTkwODZlZTAxMDM3YThmNTQ=';
const eventNames = ['evento_webhook', 'evento_webhook'];
const url = 'https://track-na.embluemail.com/contacts/event';
const cantidad = 2;
let responses = []

const ejecutarPrueba = async (eventN) => {
    for (var i = 0; i < cantidad; i++) {
        let _body = JSON.stringify({
            email: "juan" + i.toString() + '@gmail.com',
            eventName: eventN,
            attributes: { event_items: [] }
        })
        let res = await fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': credentials },
            body: _body
        })
        console.log(await res.json())
    }
};

const run = async () => {
    console.log("run");
    eventNames.forEach(event => ejecutarPrueba(event));
};

run();