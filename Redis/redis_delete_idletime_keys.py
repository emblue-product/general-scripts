import redis
import time

# Track time
start = time.time() 
total_deleted_keys = 0
total_scanned_keys = 0

# Connection to redis server
r = redis.StrictRedis(host='IP-SERVIDOR-REDIS', port=6379, db=0)
print("Redis connection status: " + str(r.ping()))

# Iterating through all redis keys
for key in r.scan_iter("*"):
    total_scanned_keys = total_scanned_keys +1
    idle = r.object("idletime", key)
    # Delete keys with idle time > 365 days (31536000 seg)
    if idle > 31536000:
      total_deleted_keys = total_deleted_keys + 1
      r.delete(key)

end = time.time()
print("Elapsed time: ", end - start)
print("Scanned keys: ", total_scanned_keys)
print("Deleted keys: ", total_deleted_keys)