const redis = require('redis');
const async = require('async');
const crypto = require('crypto');
const _ = require('lodash');
const hasher = () => crypto.createHmac('md5', '');

const StatsD = require('hot-shots');
const dogstatsd = new StatsD();

let sampleStats = async (db, key, named) => {
  let args = await db.sMembers(key);
  let itemsLen = 0;
  let hashes = {};
  await async.eachLimit(args, 128, async arg => {
    let arg2 = (named ? `${key}-` : '') + arg;
    let membersLen = await db.lLen(arg2);
    let members = (await db.lRange(arg2, 0, membersLen)).map(x => JSON.parse(x))
    let hashArray = [];
    members.forEach(member => {
      let hash = hasher().update(
        (member.AccountAutoID ? [
          member.AccountAutoID,
          member.CustomEventAutoID,
          member.ContactAutoID,
        ] : [
          member.ContactAutoID,
          member.EventAutoID,
          member.QueuedByID,
          member.FlowTraceID,
          member.EventItemAutoID,
        ]).join("")).digest("hex")
      hashArray.push(hash);
    })
    itemsLen = itemsLen + membersLen;
    hashes[arg2] = hashArray;
  })
  let obj = { key, clients: args.length, items: itemsLen, hashes: hashes };
  return obj;
}

let activitySampleStats = async (db, key) => {
  let membersLen = await db.lLen(key);
  let members = (await db.lRange(key, 0, membersLen)).map(x => JSON.parse(x))
  let hashes = {};
  let hashArray = [];
  members.forEach(member => {
    let hash = hasher().update(
      (member.AccountAutoID ? [
        member.AccountAutoID,
        member.CustomEventAutoID,
        member.ContactAutoID,
      ] : [
        member.ContactAutoID,
        member.EventAutoID,
        member.QueuedByID,
        member.FlowTraceID,
        member.EventItemAutoID,
      ]).join("")).digest("hex")
    hashArray.push(hash);
  })
  itemsLen = itemsLen + membersLen;
  hashes[key] = hashArray;

  let obj = { key, clients: args.length, items: itemsLen, hashes: hashes };
  return obj;
}


let sampleDiff = (e1, e2) => {
  let newJobs, oldJobs;

  let newJobsArray = _.flatten(Object.values(e2.hashes));
  let oldHashesArray = _.flatten(Object.values(e1.hashes));

  newJobs = newJobsArray.filter(x => !oldHashesArray.includes(x)).length;
  oldJobs = oldHashesArray.filter(x => !newJobsArray.includes(x)).length;

  return { newJobs, oldJobs };
}

let emitSampleKeys = (key, e1, e2, tags) => {
  let sample = sampleDiff(e1, e2)
  dogstatsd.gauge(`emblue.custom.automation.${key}.new_jobs`, sample.newJobs, tags)
  dogstatsd.gauge(`emblue.custom.automation.${key}.old_jobs`, sample.oldJobs, tags)
  dogstatsd.gauge(`emblue.custom.automation.${key}.total_jobs`, e2.items, tags)
  dogstatsd.gauge(`emblue.custom.automation.${key}.total_clients`, e2.clients, tags)
}

let poll = async (db0, db1, db2) => {
  console.log("before poll")
  let [events, bulk, newItems, toExecuteItems, newItemsDb2, toExecuteItemsDb2, Sent, Open,
  Click, Hater, Viral, Subscribe, Unsubscribe, Group, Tag, Item, TagByClick, ItemClicked, Events,
  ImportProcessing, Trigger, Sms, SmsClicks, ExecuteFlow, PushNotification, Bounce, FormSubscription] = await Promise.all([
    sampleStats(db0, 'eventsProcessKeys'),
    sampleStats(db0, 'BulkProcessKeys'),
    sampleStats(db1, 'flow-new-items', true),
    sampleStats(db1, 'flow-to-execute-items', true),
    sampleStats(db2, 'flow-new-items', true),
    sampleStats(db2, 'flow-to-execute-items', true), 
    activitySampleStats(db0, 'Sent'),
    activitySampleStats(db0, 'Open'),
    activitySampleStats(db0, 'Click'),
    activitySampleStats(db0, 'Hater'),
    activitySampleStats(db0, 'Viral'),
    activitySampleStats(db0, 'Subscribe'),
    activitySampleStats(db0, 'Unsubscribe'),
    activitySampleStats(db0, 'Group'),
    activitySampleStats(db0, 'Tag'),
    activitySampleStats(db0, 'Item'),
    activitySampleStats(db0, 'TagByClick'),
    activitySampleStats(db0, 'ItemClicked'),
    activitySampleStats(db0, 'Events'),
    activitySampleStats(db0, 'ImportProcessing'),
    activitySampleStats(db0, 'Trigger'),
    activitySampleStats(db0, 'Sms'),
    activitySampleStats(db0, 'SmsClicks'),
    activitySampleStats(db0, 'ExecuteFlow'),
    activitySampleStats(db0, 'PushNotification'),
    activitySampleStats(db0, 'Bounche'),
    activitySampleStats(db0, 'FormSubscription')
    
  ]);
  return { events, bulk, newItems, toExecuteItems, newItemsDb2, toExecuteItemsDb2, Sent, Open,
    Click, Hater, Viral, Subscribe, Unsubscribe, Group, Tag, Item, TagByClick, ItemClicked, Events,
    ImportProcessing, Trigger, Sms, SmsClicks, ExecuteFlow, PushNotification, Bounce, FormSubscription}
}

let e1 = null;
let fn = (async () => {
  try {
    const db0 = redis.createClient({ url: 'redis://10.200.1.180:6379', database: 0 });
    const db1 = redis.createClient({ url: 'redis://10.200.1.180:6379', database: 1 });
    const db2 = redis.createClient({ url: 'redis://10.200.1.180:6379', database: 2 });

    await db0.connect();
    await db1.connect();
    await db2.connect();

    let e2 = await poll(db0, db1, db2);
    if (e1 !== null) {
      emitSampleKeys('eventsProcessKeys', e1.events, e2.events);
      emitSampleKeys('BulkProcessKeys', e1.bulk, e2.bulk);
      emitSampleKeys('flow-new-items', e1.newItems, e2.newItems, { db: 1 });
      emitSampleKeys('flow-to-execute-items', e1.toExecuteItems, e2.toExecuteItems, { db: 1 });
      emitSampleKeys('flow-new-items', e1.newItemsDb2, e2.newItemsDb2, { db: 2 });
      emitSampleKeys('flow-to-execute-items', e1.toExecuteItemsDb2, e2.toExecuteItemsDb2, { db: 2 });      
      emitSampleKeys('Sent', e1.events, e2.events);
      emitSampleKeys('Open', e1.bulk, e2.bulk);
      emitSampleKeys('Click', e1.events, e2.events);
      emitSampleKeys('Hater', e1.bulk, e2.bulk);
      emitSampleKeys('Viral', e1.events, e2.events);
      emitSampleKeys('Subscribe', e1.bulk, e2.bulk);
      emitSampleKeys('Unsubscribe', e1.events, e2.events);
      emitSampleKeys('Group', e1.bulk, e2.bulk);
      emitSampleKeys('Tag', e1.events, e2.events);
      emitSampleKeys('Item', e1.bulk, e2.bulk);
      emitSampleKeys('TagByClick', e1.events, e2.events);
      emitSampleKeys('ItemClicked', e1.bulk, e2.bulk);
      emitSampleKeys('Events', e1.events, e2.events);
      emitSampleKeys('ImportProcessing', e1.bulk, e2.bulk);
      emitSampleKeys('Trigger', e1.events, e2.events);
      emitSampleKeys('Sms', e1.bulk, e2.bulk);
      emitSampleKeys('SmsClicks', e1.events, e2.events);
      emitSampleKeys('ExecuteFlow', e1.bulk, e2.bulk);
      emitSampleKeys('PushNotification', e1.events, e2.events);
      emitSampleKeys('FormSubscription', e1.bulk, e2.bulk);
    }
    e1 = e2;
    e2 = null;

    await db0.disconnect();
    await db1.disconnect();
    await db2.disconnect();
    
  } catch (err) {
    console.error(err);
    process.exit(0)

  }
})

setImmediate(fn)
setInterval(fn, 60000);