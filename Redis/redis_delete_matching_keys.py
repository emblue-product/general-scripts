import redis
import time

# Track time
start = time.time() 
total_deleted_keys = 0
total_scanned_keys = 0

# Connection to redis server
r = redis.StrictRedis(host='IP-SERVIDOR-REDIS', port=6379, db=0)
print("Redis connection status: " + str(r.ping()))

# Selecting all keys matching
for key in r.scan_iter("rq:job:*"):
    total_scanned_keys = total_scanned_keys +1
    idle = r.object("idletime", key)
    # Deleting all keys that contains 2839640
    if str(r.hget(key, "description")).__contains__('2839640'):
      total_deleted_keys = total_deleted_keys + 1
      print("deleting: ", key, r.hget(key, "description"))
      r.delete(key)

end = time.time()
print("Elapsed time: ", end - start)
print("Scanned keys: ", total_scanned_keys)
print("Deleted keys: ", total_deleted_keys)