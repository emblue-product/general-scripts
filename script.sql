CREATE TABLE `beep` (
  `ip` varchar(50) NOT NULL,
  `api` varchar(50) NOT NULL,
  `console_url` varchar(300) DEFAULT NULL,
  `data` varchar(2000) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `del_sms_status` (
  `status` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `role` (
  `userId` int(11) NOT NULL,
  `role` varchar(16) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`userId`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_account_apis` (
  `account` int(11) NOT NULL,
  `api` varchar(50) NOT NULL,
  `credentials` varchar(2000) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`account`),
  KEY `sms_account_gateway_sms_apis_FK` (`api`),
  CONSTRAINT `sms_account_gateway_sms_apis_FK` FOREIGN KEY (`api`) REFERENCES `sms_apis` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_apis` (
  `name` varchar(50) NOT NULL,
  `cburl` varchar(2000) NOT NULL DEFAULT 'http://localhost',
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_config` (
  `name` varchar(200) NOT NULL,
  `value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sms_country_codes` (
  `prefix` varchar(10) NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_gateway` (
  `name` varchar(50) NOT NULL,
  `api` varchar(200) NOT NULL,
  `credentials` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `sms_gateway_sms_apis_FK` (`api`),
  CONSTRAINT `sms_gateway_sms_apis_FK` FOREIGN KEY (`api`) REFERENCES `sms_apis` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_hospital` (
  `msgid` varchar(200) NOT NULL,
  `batch` varchar(200) NOT NULL DEFAULT '0',
  `gateway` varchar(200) NOT NULL,
  `original_msg` varchar(2000) NOT NULL,
  `err_msg` varchar(2000) NOT NULL,
  `ts_in` timestamp NOT NULL DEFAULT current_timestamp(),
  `ts_out` timestamp NOT NULL DEFAULT current_timestamp(),
  `worker` varchar(200) NOT NULL DEFAULT 'unknown',
  PRIMARY KEY (`msgid`,`batch`,`gateway`,`ts_in`),
  KEY `N1_sms_hospital` (`msgid`),
  KEY `N2_sms_hospital` (`ts_in`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_log_callback` (
  `bulkid` varchar(200) NOT NULL,
  `msgid` varchar(200) NOT NULL,
  `tel` varchar(500) NOT NULL,
  `gateway` varchar(200) NOT NULL,
  `original_msg` varchar(2000) NOT NULL,
  `original_status` varchar(200) NOT NULL,
  `emblue_status` varchar(200) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`msgid`,`gateway`,`ts`),
  KEY `N1_sms_log_callback` (`msgid`),
  KEY `N2_sms_log_callback` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_out` (
  `msgid` varchar(200) NOT NULL,
  `batch` varchar(200) NOT NULL DEFAULT '0',
  `gateway` varchar(200) NOT NULL,
  `tel` varchar(40) NOT NULL,
  `msg` varchar(200) NOT NULL,
  `cburl` varchar(1000) DEFAULT NULL,
  `worker` varchar(200) NOT NULL DEFAULT '0',
  `ts_in` timestamp NOT NULL DEFAULT current_timestamp(),
  `ts_out` timestamp NOT NULL DEFAULT current_timestamp(),
  `action` int(11) NOT NULL DEFAULT 0,
  `campaign` int(11) DEFAULT 0,
  `account` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`msgid`,`batch`,`gateway`,`ts_in`),
  KEY `N1_sms_out` (`msgid`),
  KEY `N2_sms_out` (`ts_in`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_route_gateway` (
  `gateway_name` varchar(50) NOT NULL,
  `country_prefix` varchar(10) NOT NULL,
  `best_price` double NOT NULL DEFAULT 9999,
  `priority` int(11) NOT NULL DEFAULT 100,
  PRIMARY KEY (`gateway_name`,`country_prefix`),
  UNIQUE KEY `sms_routes_gateway_UN` (`country_prefix`,`priority`),
  CONSTRAINT `sms_route_gateway_sms_country_codes_FK` FOREIGN KEY (`country_prefix`) REFERENCES `sms_country_codes` (`prefix`),
  CONSTRAINT `sms_route_gateway_sms_gateway_FK` FOREIGN KEY (`gateway_name`) REFERENCES `sms_gateway` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sms_routing_strategy` (
  `name` varchar(50) NOT NULL,
  `seq` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  `passHash` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='	';

CREATE TABLE `workers` (
  `id` varchar(50) NOT NULL,
  `api` varchar(50) NOT NULL,
  `keepaliveurl` varchar(200) NOT NULL,
  `gc` int(11) NOT NULL DEFAULT 0,
  `tic` timestamp NULL DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`,`api`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

