-- 0 -- CLEAN: limpiar la base antes de programar el store procedure hasta que retorne "0 row(s) afected"
DELETE FROM smsgate.sms_out WHERE ts_in < (SELECT DATE_SUB(CURDATE(), INTERVAL 15 DAY )) LIMIT 500000;

-- 1 --STORE PROCEDURE: se configura el store procedure
DELIMITER //
CREATE PROCEDURE `Clear_smsgate.sms_out` ()
BEGIN
  DECLARE total_rows INT DEFAULT 1;
  DECLARE date_limit DATE; 
  SET date_limit = (SELECT DATE_SUB(CURDATE(), INTERVAL 15 DAY ));
  WHILE total_rows > 0 DO
	  DELETE FROM smsgate.sms_out WHERE ts_in < date_limit LIMIT 500000;
	  SET total_rows = (SELECT count(1) FROM smsgate.sms_out  WHERE ts_in < date_limit);
  END WHILE;
END //
DELIMITER ;

-- 2 -- EVENT SCHEDULER CONFIGURATION: Se configura la frecuencia ejecucion del store procedure
CREATE EVENT clear_sms_out
    ON SCHEDULE EVERY 24 HOUR
    DO
      CALL `smsgate`.`Clear_smsgate.sms_out`();

